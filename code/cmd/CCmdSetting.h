/*
 * CCmdSetting.h
 *
 *  Created on: 04-01-2014
 *      Author: Przemek Kieszkowski
 */

#ifndef CCMDSETTING_H_
#define CCMDSETTING_H_

#include "CCommandsMgr.h"
#include "sysSettings.h"

#define CMD_SETTING_LIST_NAME   "setls"
#define CMD_SETTING_SET_NAME    "setset"

class CCmdSettingSet : public CCommand
{
  public:
    virtual bool start_command(CStream* stream, char* argv[], int argc);
    virtual bool run_command(CStream* stream);
    virtual bool break_command(CStream* stream);
    virtual bool is_idle() {return true;};
    virtual bool is_finished() {return true;};

    virtual void print_help(CStream* stream) {stream->printf("Modifies setting\n");};

    virtual ~CCmdSettingSet() {};
};


class CCmdSettingList : public CCommand
{
  private:
    setting_idx_t prev_idx;
    bool          finished;

  public:
    virtual bool start_command(CStream* stream, char* argv[], int argc);
    virtual bool run_command(CStream* stream);
    virtual bool break_command(CStream* stream);
    virtual bool is_idle() {return true;};
    virtual bool is_finished();

    virtual void print_help(CStream* stream) {stream->printf("Displays settings\n");};

    virtual ~CCmdSettingList() {};
};

extern CCmdSettingList  cmdSettingList;
extern CCmdSettingSet   cmdSettingSet;

#endif /* CCMDSETTING_H_ */
