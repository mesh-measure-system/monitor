/*
 * CToolCommands.cpp
 *
 *  Created on: 10 lut 2014
 *      Author: Przemek Kieszkowski
 */

#include <CToolCommands.h>
#include <CCommandsMgr.h>
#include "CLedMgr.h"
#include "CSettingsMgr.h"
#include "CArgParser.h"
#include "time.h"

CToolCommands toolCmd;

/********************************************************************************************************************
 *
 */

class CCmdAlarm : public CCommand
{
  public:
    virtual bool start_command(CStream* stream, char* argv[], int argc)
    {
      UNUSED(stream);
      UNUSED(argv);
      UNUSED(argc);

      ledMgr.ledRed.Play(&ledMgr.alarmSq);
      return false;
    };
    virtual bool is_idle() {return true;};
    virtual bool is_finished() {return true;};

    virtual void print_help(CStream* stream) {stream->printf("Starts alarm\n");};

    virtual ~CCmdAlarm() {};
};

CCmdAlarm cmdAlarm;

/********************************************************************************************************************
 *
 */
class CCmdFactory: public CCommand
{
  public:
    virtual bool start_command(CStream* stream, char* argv[], int argc)
    {
      UNUSED(stream);
      UNUSED(argv);
      UNUSED(argc);

      settingsMgr.FactorySettings();
      return false;
    };
    virtual bool is_idle() {return true;};
    virtual bool is_finished() {return true;};

    virtual void print_help(CStream* stream) {stream->printf("Resets settings to factory default\n");};

    virtual ~CCmdFactory() {};
};

CCmdFactory cmdFactory;

/********************************************************************************************************************
 *
 */
void dump_timers(void);

#define SIZE 128
class CCmdGetTime: public CCommand
{
  public:
    virtual bool start_command(CStream* stream, char* argv[], int argc)
    {
      UNUSED(stream);
      UNUSED(argv);
      UNUSED(argc);

      char buffer[SIZE];
      time_t curtime;
      struct tm *loctime;

      /* Get the current time. */
      curtime = time (NULL);

      /* Convert it to local time representation. */
      loctime = localtime (&curtime);

      /* Print it out in a nice format. */
      strftime (buffer, SIZE, "%F, %X", loctime);

      stream->printf("Current time %s\n", buffer);
      dump_timers();

      return false;
    };
    virtual bool is_idle() {return true;};
    virtual bool is_finished() {return true;};

    virtual void print_help(CStream* stream) {stream->printf("Gets current time\n");};

    virtual ~CCmdGetTime() {};
};

CCmdGetTime cmdGetTime;

class CCmdSetTime: public CCommand
{
  public:
    virtual bool start_command(CStream* stream, char* argv[], int argc)
    {
      // no additional options for set setting
      CArgParser  argP(argv, argc, F(" "));
      int         c;
      bool        finish = false;
      while (!finish)
      {
        c = argP.getSwitch();
        switch (c)
        {
          case CArgParser::CARG_INVALID_ARG:
          default:
          {
            break;
          }
          case CArgParser::CARG_END_OF_SWITCH:
          {
            finish = true;
            break;
          }
        }
      }

      if ((argP.not_switch_idx > 0))
      {
        struct tm rtc_time;

        int tm_year;
        int tm_mon;
        int tm_mday;
        int tm_hour;
        int tm_min;
        int tm_sec;

        int num = sscanf(argv[0], "%d-%d-%d %d:%d:%d",
            &tm_year, &tm_mon, &tm_mday,
            &tm_hour, &tm_min, &tm_sec);

        rtc_time.tm_isdst = 0;
        rtc_time.tm_year  = tm_year - 1900;
        rtc_time.tm_mon   = tm_mon - 1;
        rtc_time.tm_mday  = tm_mday;
        rtc_time.tm_hour  = tm_hour;
        rtc_time.tm_min   = tm_min;
        rtc_time.tm_sec   = tm_sec;

        if (num == 6)
        {
          time_t new_time = mktime(&rtc_time);

          if (new_time != (time_t)(-1))
          {
            char buffer[128];
            set_system_time(new_time);

            /* Print it out in a nice format. */
            strftime(buffer, sizeof(buffer), "%F, %X\n", &rtc_time);
            stream->printf("Current time is %s\n", buffer);
          }
          else
          {
            stream->printf(F("Bad params\n"));
          }
        }
        else
        {
          stream->printf(F("Not enought command params (%d)\n"), num);
        }

      }
      else
      {
        stream->printf(F("Not proper command params\n"));
      }
      return false;
    };
    virtual bool is_idle() {return true;};
    virtual bool is_finished() {return true;};

    virtual void print_help(CStream* stream) {stream->printf("Sets current time\n");};

    virtual ~CCmdSetTime() {};
};

CCmdSetTime cmdSetTime;


void CToolCommands::Init(void)
{
  cmdAlarm.init((char*)"alarm");
  cmdFactory.init((char*)"factory-reset");
  cmdGetTime.init((char*)"gettime");
  cmdSetTime.init((char*)"settime");
}

