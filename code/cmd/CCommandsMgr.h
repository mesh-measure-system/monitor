/*
 * CCommandsMgr.h
 *
 *  Created on: 27-11-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CCOMMANDSMGR_H_
#define CCOMMANDSMGR_H_

#include <compiler.h>
#include "CRun.h"
#include "CStream.h"
#include "CList.h"

#define CCMD_MAX_COMMAND_LENGTH   128
#define CCMD_MAX_ARGUMENTS_COUNT  20

class CCommandsMgr;
class CCommand;

class CCommand : public CListItem<CCommand>
{
  private:
    char*     command_name;

  public:
    virtual void init(char* cmd_name);
    virtual bool start_command(CStream* stream, char* argv[CCMD_MAX_ARGUMENTS_COUNT], int argc) = 0;
    virtual bool run_command(CStream* stream) {UNUSED(stream); return true;};
    virtual bool break_command(CStream* stream) {UNUSED(stream); return true;};

    // function called by command manager to decide if current command is finished and execution can be passed to the next command
    virtual bool is_finished() {return true;};
    virtual bool is_idle() {return true;};

    virtual void print_help(CStream* stream) = 0;

    bool operator==(char* cmd_name);

    char* getCommandName();

    friend class CCommandsMgr;
    friend class CHelpCommand;
};

class CCommandsMgr : public CRun
{
  public:
    typedef enum
    {
      PARSE_OK,
      PARSE_UNTERMINATED_SINGLE_QUOTE,
      PARSE_UNTERMINATED_DOUBLE_QUOTE,
      PARSE_UNTERMINATED_ESCAPE_CHAR,
      PARSE_UNKNOWN_ESCAPE_CHAR,
      PARSE_TOO_LARGE_ARGUMENTS_NUMBER,
    }command_parse_error_t;

  private:
//    CCommand* registered_commands;
    CCommand* current_command;
    CStream*  current_stream;
    CStream*  output_stream;

    CList<CCommand> command_list;

    int parseCommand(char* command_p, command_parse_error_t* error_code_p, char* argv_p[], int* argc_p, char* arguments_p);
    int parseSingleQuote(char* command, command_parse_error_t* error_code, char** arguments_pp);
    int parseDoubleQuote(char* command, command_parse_error_t* error_code, char** arguments_pp);
    int parseEscape(char* command, command_parse_error_t* error_code, char** arguments_pp);
    CCommand* findCommand(char* command);

  public:
    void Init();
    virtual void run();
    virtual bool isIdle();

    void registerCommand(CCommand* cmd_p);

    // function called by command source stream to execute received command
    bool exec_commands(char* command, CStream* stream);

    CStream* getOutStream(void);

    int cmdParams(char* argv[], int argc, char* params);
    virtual ~CCommandsMgr() {};

    friend class CHelpCommand;
};

extern CCommandsMgr cmdMgr;

#endif /* CCOMMANDSMGR_H_ */
