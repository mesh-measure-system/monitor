/*
 * CNetwork.cpp
 *
 *  Created on: 1 pa� 2014
 *      Author: Przemek
 */

#include <string.h>
#include "CNetwork.h"
#include "avr2025_mac.h"
#include "common_sw_timer.h"
#include "CCommandsMgr.h"
#include "CArgParser.h"

#include "beacon_app.h"

//#undef TRACE_LEVEL
//#define TRACE_LEVEL TRACE_LEVEL_ERROR

#include "trace.h"

CNetwork nwk;


/* === MACROS ============================================================== */

#define CHANNEL_OFFSET                  (0)

/** Defines the default Beacon Order. */
#define DEFAULT_BO                      (6)
/** Defines the default Super frame Order. */
#define DEFAULT_SO                      (4)

/** Coord Realignment is not used by default in this application. */
#define DEFAULT_COORDREALIGNMENT                (false)

/** Defines the default Coord Realign SecurityLevel */
#define DEFAULT_COORDREALIGN_SEC_LVL    (0x00)

/** Defines the default Coord Realign KeyIdMode */
#define DEFAULT_COORDREALIGN_KEYID_MODE (0x00)

/** Defines the default Coord Realign KeySource */
#define DEFAULT_COORDREALIGN_KEY_SRC    (NULL)

/** Defines the default Coord Realign KeyIndex */
#define DEFAULT_COORDREALIGN_KEY_INDEX  (0x00)

/** Defines the default Beacon Security Level */
#define DEFAULT_BEACON_SEC_LVL                  (0x05)

/** Defines the default Beacon KeyIdMode */
#define DEFAULT_BEACON_KEYID_MODE               (0x01)

/** Defines the default Beacon KeySource */
#define DEFAULT_BEACON_KEY_SRC                  (default_key_source)

/** Defines the default Beacon KeyIndex */
#define DEFAULT_BEACON_KEY_INDEX                (0x00)

/**
 * Defines the time in ms to initiate a broadcast data transmission
 * to all devices.
 */
#define APP_BC_DATA_DURATION_MS         (9000)

/** Defines the time to initiate a indirect data transmission to the device. */
#define APP_INDIRECT_DATA_DURATION_MS   (6000)
#ifdef GTS_SUPPORT
/** Defines the time to initiate a GTS data transmission to the device. */
#define APP_GTS_DATA_DURATION_MS        (8000)
#endif
#define DEBOUNCE_DELAY_MS               (200)


/* === GLOBALS ============================================================= */
extern uint8_t default_key_source[8];
/** This array stores all device related information. */
associated_device_t device_list[MAX_NUMBER_OF_DEVICES];
/** Stores the number of associated devices. */
uint16_t no_of_assoc_devices;


/** This array stores the current beacon payload. */
char*   beacon_payload[20];
uint8_t beacon_payload_len;

#ifdef GPIO_PUSH_BUTTON_0
static uint8_t broadcast_payload[] = {"Broadcast Data"};
#endif
#ifdef GTS_SUPPORT
static uint8_t gts_payload[] = {"GTS Data from coordinator"};
#endif /* GTS_SUPPORT */
/** This variable stores the current state of the node. */
coord_state_t coord_state = COORD_STARTING;
/** This variable counts the number of transmitted data frames. */
//static uint32_t tx_cnt;
/** Store the current MSDU handle to be used for a data frame. */
//static uint8_t curr_msdu_handle;
#ifdef GTS_SUPPORT
static uint8_t gts_msdu_handle;
#endif /* GTS_SUPPORT */
uint8_t current_channel;
uint8_t current_channel_page;
static uint32_t channels_supported;
#ifdef GTS_SUPPORT
static uint8_t APP_TIMER_GTS_DATA;
#endif /* GTS_SUPPORT */
static uint8_t APP_TIMER_INDIRECT_DATA;
static uint8_t APP_TIMER_BC_DATA;
#ifdef GPIO_PUSH_BUTTON_0
static wpan_addr_spec_t dst_addr;
#endif /* GPIO_PUSH_BUTTON_0 */

#ifdef MAC_SECURITY_ZIP
/** Store the recently associated device number */
uint16_t recent_assoc_dev_no = 0xFFFF;
#endif

/* === PROTOTYPES ========================================================== */

/**
 * @brief Application specific function to assign a short address
 */
static bool assign_new_short_addr(uint64_t addr64, uint16_t *addr16);

// /**
//  * @brief Callback function for initiation of broadcast data transmission
//  *
//  * @param parameter Pointer to callback parameter
//  *                  (not used in this application, but could be used
//  *                  to indicated LED to be switched off)
//  */
// static void bc_data_cb(void *parameter);

/**
 * @brief Callback function for initiation of indirect data transmission
 *
 * @param parameter Pointer to callback parameter
 *                  (not used in this application, but could be used
 *                  to indicated LED to be switched off)
 */
//static void indirect_data_cb(void *parameter);

/**
 * @brief Callback function for initiation of gts data transmission
 *
 * @param parameter Pointer to callback parameter
 *                  (not used in this application, but could be used
 *                  to indicated LED to be switched off)
 */
#ifdef GTS_SUPPORT
static void gts_data_cb(void *parameter);
#endif

/** Alert to indicate something has gone wrong in the application */
//static void app_alert(void);

/** This function shows the stack and application
 *  capabilities on terminal if SIO_HUB switch
 *  is enabled.
 */
//static void print_stack_app_build_features(void);

/* === IMPLEMENTATION ====================================================== */

/*********************************************************************************************************************/
/*
 *
 */
class CDevListCmd: public CCommand
{
  public:
  bool cmd_running;

  uint16_t curr_dev;

  virtual bool start_command(CStream* stream, char* argv[], int argc)
  {
    CArgParser  argP(argv, argc, F(" "));
    int         c;
    bool        finish = false;

    while (!finish)
    {
      c = argP.getSwitch();
      switch (c)
      {
//         case 'l':
//         {
//           break;
//         }
        case CArgParser::CARG_INVALID_ARG:
        default:
        {
          break;
        }
        case CArgParser::CARG_END_OF_SWITCH:
        {
          finish = true;
          break;
        }
      }
    }

    curr_dev = 0;

    if (no_of_assoc_devices > curr_dev)
    {
      cmd_running = true;
    }

    if (!cmd_running)
    {
      stream->printf(F("End\n"));
    }
    return cmd_running;
  };

  virtual bool run_command(CStream* stream)
  {
    if (no_of_assoc_devices > curr_dev)
    {
      stream->printf("MAC:0x%08lx%08lx SHORT:0x%x\n", (uint32_t)(device_list[curr_dev].ieee_addr >> 32), (uint32_t)device_list[curr_dev].ieee_addr, (uint16_t)device_list[curr_dev].short_addr);
      curr_dev++;
    }

    if (no_of_assoc_devices > curr_dev)
    {
      cmd_running = true;
    }
    else
    {
      cmd_running = false;
    }

    if (!cmd_running)
    {
      stream->printf(F("End\n"));
    }
    return cmd_running;
  }
  virtual bool break_command(CStream* stream)
  {
    cmd_running = false;
    stream->printf(F("Break\n"));
    return false;
  }
  virtual bool is_idle() {return !cmd_running;};
  virtual bool is_finished() {return !cmd_running;};
  virtual void print_help(CStream* stream) {stream->printf("Lists associated devices\n");};
  virtual ~CDevListCmd() {};
};

CDevListCmd cmdDevList;


/**
 * @brief Main function of the device application
 */

void CNetwork::Init()
{
  retval_t  mac_ret;
  CRun::Init();

  mac_hi.Init((char*)"MAC_HI", 0x00000000LL);
  mac_lo.Init((char*)"MAC_LO", 1001LL);

  // setup beacon command
  cnet_frame_header_t*  fh = (cnet_frame_header_t*)&beacon_payload[0];
  fh->srcEndPoint   = CNET_DEFAULT_BEACON_ENDPOINT_ID;
  fh->dstEndPoint   = CNET_DEFAULT_BEACON_ENDPOINT_ID;
  fh->control       = CNET_FH_CTRL_END_COMMAND;

  int len = snprintf((char*)beacon_payload + sizeof(cnet_frame_header_t), sizeof(beacon_payload) - sizeof(cnet_frame_header_t), "Test beacon");
  beacon_payload_len = len + sizeof(cnet_frame_header_t);

  mac_ret = wpan_init(((uint64_t)mac_hi.get() << 32) + (uint64_t)mac_lo.get());
  if (MAC_SUCCESS != mac_ret)
  {
    TRACE_ERROR("Error MAC initializing = 0x%x\n", (int)mac_ret);
  }

	sw_timer_get_id(&APP_TIMER_INDIRECT_DATA);
	sw_timer_get_id(&APP_TIMER_BC_DATA);
#ifdef GTS_SUPPORT
	sw_timer_get_id(&APP_TIMER_GTS_DATA);
#endif

	/*
	 * Reset the MAC layer to the default values.
	 * This request will cause a mlme reset confirm message ->
	 * usr_mlme_reset_conf
	 */
	wpan_mlme_reset_req(true);
#ifdef ENABLE_SLEEP
  sw_timer_get_id(&APP_TIMER_SLEEP);
#endif

  cmdDevList.init((char*)"lsdev");
}


void CNetwork::run()
{
  wpan_task();
}

bool CNetwork::isIdle()
{
  return false;
}

#if defined(ENABLE_TSTAMP)

/*
 * Callback function usr_mcps_data_conf
 *
 * @param msduHandle  Handle of MSDU handed over to MAC earlier
 * @param status      Result for requested data transmission request
 * @param Timestamp   The time, in symbols, at which the data were transmitted
 *                    (only if timestamping is enabled).
 *
 */
#if 0
#ifdef SIO_HUB
const char Dispaly_Result_Frame[] = "Result frame with handle %u : ";
#endif

void usr_mcps_data_conf(uint8_t msduHandle, uint8_t status,	uint32_t Timestamp)
#else
void usr_mcps_data_conf(uint8_t msduHandle,	uint8_t status)
#endif  /* ENABLE_TSTAMP */
{
	if (status == MAC_SUCCESS)
  {
		TRACE_USER("Success\r\n");
	}
  else if (status == MAC_TRANSACTION_OVERFLOW)
  {
		/* Frame could not be placed into the indirect queue. */
		TRACE_USER("Transaction overflow\r\n");
	}
  else if (status == MAC_TRANSACTION_EXPIRED)
  {

		/*
		 * Frame could not be delivered to the target node within
		 * the proper time.
		 */
		TRACE_USER("Transaction expired\r\n");
	}
  else if (status == MAC_NO_ACK)
  {

		/*
		 * Frame could not be delivered to the target node within
		 * the proper time.
		 */
		TRACE_USER("Frame Transmitted MAC No Ack\r\n");
	}
  else if (status == MAC_CHANNEL_ACCESS_FAILURE)
  {

		/*
		 * Frame could not be delivered to the target node within
		 * the proper time.
		 */
		TRACE_USER("MAC Channel Access Failure\r\n");
	}
}
#endif

#if ((MAC_PURGE_REQUEST_CONFIRM == 1) && (MAC_INDIRECT_DATA_BASIC == 1))

/*
 * Callback function usr_mcps_purge_conf
 *
 * @param msduHandle           Handle (id) of MSDU to be purged.
 * @param status               Result of requested purge operation.
 *
 * @return void
 *
 */
void usr_mcps_purge_conf(uint8_t msduHandle, uint8_t status)
{
	TRACE_USER("usr_mcps_purge_conf\n");
}

#endif  /* ((MAC_PURGE_REQUEST_CONFIRM == 1) && (MAC_INDIRECT_DATA_BASIC == 1))
        **/

#if (MAC_ASSOCIATION_REQUEST_CONFIRM == 1)

/*
 * Callback function usr_mlme_associate_conf.
 *
 * @param AssocShortAddress    Short address allocated by the coordinator.
 * @param status               Result of requested association operation.
 *
 * @return void
 *
 */
void usr_mlme_associate_conf(uint16_t AssocShortAddress, uint8_t status)
{
    TRACE_USER("Connected to beacon-enabled network addr= 0x%04x\r\n", AssocShortAddress);
	/* Keep compiler happy. */
	AssocShortAddress = AssocShortAddress;
}

#endif  /* (MAC_ASSOCIATION_REQUEST_CONFIRM == 1) */
#ifdef GTS_SUPPORT
void usr_mlme_gts_conf(gts_char_t GtsChar, uint8_t status)
{
	status = status;
}

#endif
#ifdef GTS_SUPPORT
void usr_mlme_gts_ind(uint16_t DeviceAddr, gts_char_t GtsChar)
{
	TRACE_USER("usr_mlme_gts_ind\n");
	sw_timer_start(APP_TIMER_GTS_DATA, ((uint32_t)APP_GTS_DATA_DURATION_MS * 1000), SW_TIMEOUT_RELATIVE, (FUNC_PTR)gts_data_cb, NULL);
	DeviceAddr = DeviceAddr;
	GtsChar = GtsChar;
}

#endif
#if (MAC_ASSOCIATION_INDICATION_RESPONSE == 1)

/*
 * @brief Callback function usr_mlme_associate_ind
 *
 * @param DeviceAddress         Extended address of device requesting
 * association
 * @param CapabilityInformation Capabilities of device requesting association
 */
void usr_mlme_associate_ind(uint64_t DeviceAddress, uint8_t CapabilityInformation)
{
//	TRACE_USER("usr_mlme_associate_ind\n");
	/*
	 * Any device is allowed to join the network.
	 * Use: bool wpan_mlme_associate_resp(uint64_t DeviceAddress,
	 *                                    uint16_t AssocShortAddress,
	 *                                    uint8_t status);
	 *
	 * This response leads to comm status indication ->
	 * usr_mlme_comm_status_ind
	 * Get the next available short address for this device.
	 */
	uint16_t associate_short_addr = macShortAddress_def;

	if (assign_new_short_addr(DeviceAddress, &associate_short_addr) == true)
  {
		wpan_mlme_associate_resp(DeviceAddress, associate_short_addr, ASSOCIATION_SUCCESSFUL);
	}
  else
  {
		wpan_mlme_associate_resp(DeviceAddress, associate_short_addr, PAN_AT_CAPACITY);
	}

	/* Keep compiler happy. */
	CapabilityInformation = CapabilityInformation;
}

#endif  /* (MAC_ASSOCIATION_INDICATION_RESPONSE == 1) */

#if (MAC_BEACON_NOTIFY_INDICATION == 1)

/*
 * Callback function usr_mlme_beacon_notify_ind
 *
 * @param BSN            Beacon sequence number.
 * @param PANDescriptor  Pointer to PAN descriptor for received beacon.
 * @param PendAddrSpec   Pending address specification in received beacon.
 * @param AddrList       List of addresses of devices the coordinator has
 * pending data.
 * @param sduLength      Length of beacon payload.
 * @param sdu            Pointer to beacon payload.
 *
 * @return void
 *
 */
void usr_mlme_beacon_notify_ind(uint8_t BSN, wpan_pandescriptor_t *PANDescriptor, uint8_t PendAddrSpec, uint8_t *AddrList, uint8_t sduLength,	uint8_t *sdu)
{
	TRACE_USER("usr_mlme_beacon_notify_ind\n");
}

#endif  /* (MAC_BEACON_NOTIFY_INDICATION == 1) */

#if ((MAC_ORPHAN_INDICATION_RESPONSE == 1) || \
	(MAC_ASSOCIATION_INDICATION_RESPONSE == 1))

/*
 * @brief Callback function usr_mlme_comm_status_ind
 *
 * @param SrcAddrSpec      Pointer to source address specification
 * @param DstAddrSpec      Pointer to destination address specification
 * @param status           Result for related response operation
 */
void usr_mlme_comm_status_ind(wpan_addr_spec_t *SrcAddrSpec, wpan_addr_spec_t *DstAddrSpec, uint8_t status)
{
	if (status == MAC_SUCCESS)
  {
     switch (DstAddrSpec->AddrMode)
    {
      case WPAN_ADDRMODE_NONE:
      {
        TRACE_USER("New device associated WPAN_ADDRMODE_NONE\n");
        break;
      }
      case WPAN_ADDRMODE_SHORT:
      {
        TRACE_USER("New device associated WPAN_ADDRMODE_SHORT 0x%04x\n", DstAddrSpec->Addr.short_address);
        break;
      }
      case WPAN_ADDRMODE_LONG:
      {
	      TRACE_USER("New device associated WPAN_ADDRMODE_LONG 0x%08lx%08lx\n", (uint32_t)(DstAddrSpec->Addr.long_address >> 32), (uint32_t)DstAddrSpec->Addr.long_address);
        break;
      }
    }
  }
  else
  {
    TRACE_USER("Problem with association %d\n", (int)status);
  }


	if (status == MAC_SUCCESS)
  {

#if (defined MAC_SECURITY_ZIP) || (defined MAC_SECURITY_2006)
		recent_assoc_dev_no++;
		wpan_mlme_set_req(macDeviceTableEntries, NO_PIB_INDEX, &no_of_assoc_devices);
		wpan_mlme_get_req(macKeyTable, recent_assoc_dev_no);
#endif

// 		/*
// 		 * Now the association of the device has been successful and its
// 		 * information, like address, could  be stored.
// 		 * But for the sake of simple handling it has been done
// 		 * during assignment of the short address within the function
// 		 * assign_new_short_addr()
// 		 */
//
// 		/* Start timer to initiate indirect data transmission. */
// 		sw_timer_start(APP_TIMER_INDIRECT_DATA,
// 				((uint32_t)APP_INDIRECT_DATA_DURATION_MS * 1000),
// 				SW_TIMEOUT_RELATIVE,
// 				(FUNC_PTR)indirect_data_cb,
// 				NULL);
	}
  else
  {
	}

	/* Keep compiler happy. */
	SrcAddrSpec = SrcAddrSpec;
	DstAddrSpec = DstAddrSpec;
}

#endif  /* ((MAC_ORPHAN_INDICATION_RESPONSE == 1) ||
         *(MAC_ASSOCIATION_INDICATION_RESPONSE == 1)) */

#if (MAC_DISASSOCIATION_BASIC_SUPPORT == 1)

/*
 * Callback function usr_mlme_disassociate_conf
 *
 * @param status             Result of requested disassociate operation.
 * @param DeviceAddrSpec     Pointer to wpan_addr_spec_t structure for device
 *                           that has either requested disassociation or been
 *                           instructed to disassociate by its coordinator.
 *
 * @return void
 */
void usr_mlme_disassociate_conf(uint8_t status, wpan_addr_spec_t *DeviceAddrSpec)
{
	UNUSED(status);
	UNUSED(DeviceAddrSpec);
	TRACE_USER("usr_mlme_disassociate_conf\n");
}

#endif /* (MAC_DISASSOCIATION_BASIC_SUPPORT == 1)*/

#if (MAC_DISASSOCIATION_BASIC_SUPPORT == 1)

/*
 * Callback function usr_mlme_disassociate_ind
 *
 * @param DeviceAddress        Extended address of device which initiated the
 *                             disassociation request.
 * @param DisassociateReason   Reason for the disassociation. Valid values:
 *                           - @ref WPAN_DISASSOC_BYPARENT,
 *                           - @ref WPAN_DISASSOC_BYCHILD.
 *
 * @return void
 */
void usr_mlme_disassociate_ind(uint64_t DeviceAddress, uint8_t DisassociateReason)
{
	UNUSED(DeviceAddress);
	UNUSED(DisassociateReason);
	TRACE_USER("usr_mlme_disassociate_ind\n");
}

#endif  /* (MAC_DISASSOCIATION_BASIC_SUPPORT == 1) */

#if (MAC_GET_SUPPORT == 1)

/*
 * Callback function usr_mlme_get_conf
 *
 * @param status            Result of requested PIB attribute get operation.
 * @param PIBAttribute      Retrieved PIB attribute.
 * @param PIBAttributeIndex Index of the PIB attribute to be read.
 * @param PIBAttributeValue Pointer to data containing retrieved PIB attribute,
 *
 * @return void
 */
void usr_mlme_get_conf(uint8_t status,uint8_t PIBAttribute,
#if (defined MAC_SECURITY_ZIP) || (defined MAC_SECURITY_2006)
		uint8_t PIBAttributeIndex,
#endif
		void *PIBAttributeValue)
{
//	TRACE_USER("usr_mlme_get_conf\n");

#if (defined MAC_SECURITY_ZIP) || (defined MAC_SECURITY_2006)
	mac_key_table_t *key_table = (mac_key_table_t *)PIBAttributeValue;
#endif
	if ((status == MAC_SUCCESS) && (PIBAttribute == phyCurrentPage))
  {
#ifdef HIGH_DATA_RATE_SUPPORT
		current_channel_page = 17;
#else
		current_channel_page = *(uint8_t *)PIBAttributeValue;
#endif

#if (defined MAC_SECURITY_ZIP) || (defined MAC_SECURITY_2006)
		wpan_mlme_get_req(phyChannelsSupported, 0);
#else
		wpan_mlme_get_req(phyChannelsSupported);
#endif
	}
  else if ((status == MAC_SUCCESS) &&	(PIBAttribute == phyChannelsSupported))
  {
		uint8_t index;

		channels_supported = convert_byte_array_to_32_bit(PIBAttributeValue);

		for (index = 0; index < 32; index++)
    {
			if (channels_supported & (1 << index))
      {
				current_channel = index + CHANNEL_OFFSET;
				break;
			}
		}

		/*
		 * Set the short address of this node.
		 * Use: bool wpan_mlme_set_req(uint8_t PIBAttribute,
		 *                             void *PIBAttributeValue);
		 *
		 * This request leads to a set confirm message ->
		 * usr_mlme_set_conf
		 */

		/* MAC Short Address will be set after setting the Security
		 * PIB's */
#if (defined MAC_SECURITY_ZIP) || (defined MAC_SECURITY_2006)
		/* Set security PIB attributes now. */
		wpan_mlme_set_req(macDefaultKeySource, NO_PIB_INDEX, &default_key_source);
#else

		uint8_t short_addr[2];

		short_addr[0] = (uint8_t)COORD_SHORT_ADDR;  /* low byte */
		short_addr[1] = (uint8_t)(COORD_SHORT_ADDR >> 8); /* high byte
		                                                  **/

		wpan_mlme_set_req(macShortAddress, short_addr);
#endif /* (defined MAC_SECURITY_ZIP) || (defined MAC_SECURITY_2006) */
	}

#ifdef MAC_SECURITY_ZIP
	else if ((status == MAC_SUCCESS) &&	(PIBAttribute == macKeyTable))
  {
		for (uint8_t j = 0; j < key_table->KeyDeviceListEntries; j++)
    {
			if (EMPTY_DEV_HANDLE ==	(key_table->KeyDeviceList[j].DeviceDescriptorHandle))
      {
				key_table->KeyDeviceList[j].DeviceDescriptorHandle = recent_assoc_dev_no;
				key_table->KeyDeviceList[j].UniqueDevice = true;
				break;
			}
		}
		wpan_mlme_set_req(macKeyTable, PIBAttributeIndex,	(uint8_t *)PIBAttributeValue);
	}
#endif /* (defined MAC_SECURITY_ZIP) || (defined MAC_SECURITY_2006) */
}

#endif  /* (MAC_GET_SUPPORT == 1) */

#if (MAC_ORPHAN_INDICATION_RESPONSE == 1)

/*
 * Callback function usr_mlme_orphan_ind
 *
 * @param OrphanAddress     Address of orphaned device.
 *
 * @return void
 *
 */
void usr_mlme_orphan_ind(uint64_t OrphanAddress)
{
    TRACE_USER("usr_mlme_orphan_ind\n");
}

#endif  /* (MAC_ORPHAN_INDICATION_RESPONSE == 1) */

#if (MAC_INDIRECT_DATA_BASIC == 1)

/*
 * Callback function that must be implemented by application (NHLE) for MAC
 * service
 * MLME-POLL.confirm.
 *
 * @param status           Result of requested poll operation.
 *
 * @return void
 *
 */
void usr_mlme_poll_conf(uint8_t status)
{
	UNUSED(status);
//	TRACE_USER("usr_mlme_poll_conf\n");
}

#endif  /* (MAC_INDIRECT_DATA_BASIC == 1) */

/*
 * @brief Callback function usr_mlme_reset_conf
 *
 * @param status Result of the reset procedure
 */
void usr_mlme_reset_conf(uint8_t status)
{
//  TRACE_USER("usr_mlme_reset_conf %d\n", (int ) status);
	if (status == MAC_SUCCESS)
  {
#if (defined MAC_SECURITY_ZIP) || (defined MAC_SECURITY_2006)
		wpan_mlme_get_req(phyCurrentPage, 0);
#else
		wpan_mlme_get_req(phyCurrentPage);
#endif
	}
  else
  {
		/* Something went wrong; restart. */
		wpan_mlme_reset_req(true);
	}
}

#if (MAC_RX_ENABLE_SUPPORT == 1)

/*
 * Callback function usr_mlme_rx_enable_conf
 *
 * @param status           Result of requested receiver enable operation.
 *
 * @return void
 */
void usr_mlme_rx_enable_conf(uint8_t status)
{
	UNUSED(status);
//	TRACE_USER("usr_mlme_rx_enable_conf\n");
}

#endif  /* (MAC_RX_ENABLE_SUPPORT == 1) */

#if ((MAC_SCAN_ED_REQUEST_CONFIRM == 1)      ||	\
	(MAC_SCAN_ACTIVE_REQUEST_CONFIRM == 1)  || \
	(MAC_SCAN_PASSIVE_REQUEST_CONFIRM == 1) || \
	(MAC_SCAN_ORPHAN_REQUEST_CONFIRM == 1))

/*
 * @brief Callback function usr_mlme_scan_conf
 *
 * @param status            Result of requested scan operation
 * @param ScanType          Type of scan performed
 * @param ChannelPage       Channel page on which the scan was performed
 * @param UnscannedChannels Bitmap of unscanned channels
 * @param ResultListSize    Number of elements in ResultList
 * @param ResultList        Pointer to array of scan results
 */
void usr_mlme_scan_conf(uint8_t status, uint8_t ScanType, uint8_t ChannelPage, uint32_t UnscannedChannels, uint8_t ResultListSize, void *ResultList)
{
//	TRACE_USER("usr_mlme_scan_conf\n");
	/*
	 * We are not interested in the actual scan result,
	 * because we start our network on the pre-defined channel anyway.
	 * Start a beacon-enabled network
	 * Use: bool wpan_mlme_start_req(uint16_t PANId,
	 *                               uint8_t LogicalChannel,
	 *                               uint8_t ChannelPage,
	 *                               uint8_t BeaconOrder,
	 *                               uint8_t SuperframeOrder,
	 *                               bool PANCoordinator,
	 *                               bool BatteryLifeExtension,
	 *                               bool CoordRealignment)
	 *
	 * This request leads to a start confirm message -> usr_mlme_start_conf
	 */
	wpan_mlme_start_req(DEFAULT_PAN_ID,	current_channel, current_channel_page, DEFAULT_BO, DEFAULT_SO, true, false, DEFAULT_COORDREALIGNMENT

#ifdef MAC_SECURITY_BEACON
			, DEFAULT_COORDREALIGN_SEC_LVL,
			DEFAULT_COORDREALIGN_KEYID_MODE,
			DEFAULT_COORDREALIGN_KEY_SRC,
			DEFAULT_COORDREALIGN_KEY_INDEX,
			DEFAULT_BEACON_SEC_LVL,
			DEFAULT_BEACON_KEYID_MODE,
			DEFAULT_BEACON_KEY_SRC,
			DEFAULT_BEACON_KEY_INDEX
#endif
			);

	/* Keep compiler happy. */
	status = status;
	ScanType = ScanType;
	ChannelPage = ChannelPage;
	UnscannedChannels = UnscannedChannels;
	ResultListSize = ResultListSize;
	ResultList = ResultList;
}

#endif

#ifndef MAC_SECURITY_ZIP
void usr_mlme_set_conf(uint8_t status, uint8_t PIBAttribute)
{
//	TRACE_USER("usr_mlme_set_conf status = %d\n", (int)status);
	if ((status == MAC_SUCCESS) && (PIBAttribute == macShortAddress))
  {
		/*
		 * Allow other devices to associate to this coordinator.
		 * Use: bool wpan_mlme_set_req(uint8_t PIBAttribute,
		 *                             void *PIBAttributeValue);
		 *
		 * This request leads to a set confirm message ->
		 * usr_mlme_set_conf
		 */
		uint8_t association_permit = true;

		wpan_mlme_set_req(macAssociationPermit,	&association_permit);
	}
  else if ((status == MAC_SUCCESS) &&	(PIBAttribute == macAssociationPermit))
  {
		/*
		 * Set RX on when idle to enable the receiver as default.
		 * Use: bool wpan_mlme_set_req(uint8_t PIBAttribute,
		 *                             void *PIBAttributeValue);
		 *
		 * This request leads to a set confirm message ->
		 * usr_mlme_set_conf
		 */
		bool rx_on_when_idle = false;

		wpan_mlme_set_req(macRxOnWhenIdle, &rx_on_when_idle);
	}
  else if ((status == MAC_SUCCESS) &&	(PIBAttribute == macRxOnWhenIdle))
  {
		/* Set the beacon payload length. */
		uint8_t tmp_beacon_payload_len = beacon_payload_len;
		wpan_mlme_set_req(macBeaconPayloadLength,	&tmp_beacon_payload_len);
	}
  else if ((status == MAC_SUCCESS) && (PIBAttribute == macBeaconPayloadLength))
  {
		/*
		 * Once the length of the beacon payload has been defined,
		 * set the actual beacon payload.
		 */
		wpan_mlme_set_req(macBeaconPayload,	&beacon_payload);
	}
  else if ((status == MAC_SUCCESS) && (PIBAttribute == macBeaconPayload))
  {
		if (COORD_STARTING == coord_state)
    {
			/*
			 * Initiate an active scan over all channels to
			 * determine
			 * which channel to use.
			 * Use: bool wpan_mlme_scan_req(uint8_t ScanType,
			 *                              uint32_t ScanChannels,
			 *                              uint8_t ScanDuration,
			 *                              uint8_t ChannelPage);
			 *
			 * This request leads to a scan confirm message ->
			 * usr_mlme_scan_conf
			 * Scan for about 50 ms on each channel -> ScanDuration
			 *= 1
			 * Scan for about 1/2 second on each channel ->
			 * ScanDuration = 5
			 * Scan for about 1 second on each channel ->
			 * ScanDuration = 6
			 */
			wpan_mlme_scan_req(MLME_SCAN_TYPE_ACTIVE,	SCAN_CHANNEL(current_channel), SCAN_DURATION_COORDINATOR,	current_channel_page);
		}
    else
    {
			/* Do nothing once the node is properly running. */
		}
	}
  else
  {
		/* Something went wrong; restart. */
		wpan_mlme_reset_req(true);
	}
}

#endif

/*
 * @brief Callback function usr_mlme_start_conf
 *
 * @param status        Result of requested start operation
 */
#if (MAC_START_REQUEST_CONFIRM == 1)
void usr_mlme_start_conf(uint8_t status)
{
//	TRACE_USER("usr_mlme_start_conf\n");
	if (status == MAC_SUCCESS)
  {
		coord_state = COORD_RUNNING;

#ifdef SIO_HUB
		printf("Started beacon-enabled network in Channel - %d\r\n", current_channel);
#endif

		/*
		 * Network is established.
		 * Waiting for association indication from a device.
		 * -> usr_mlme_associate_ind
		 */
//		TRACE_USER("LED_On(LED_NWK_SETUP)\n");

		/*
		 * Now that the network has been started successfully,
		 * the timer for broadcast data transmission is started.
		 * This is independent from the actual number of associated
		 * nodes.
		 */

// 		/* Start timer to initiate broadcast data transmission. */
// 		sw_timer_start(APP_TIMER_BC_DATA,
// 				((uint32_t)APP_BC_DATA_DURATION_MS * 1000),
// 				SW_TIMEOUT_RELATIVE,
// 				(FUNC_PTR)bc_data_cb,
// 				NULL);

	}
  else
  {
		/* Something went wrong; restart. */
		wpan_mlme_reset_req(true);
	}
}

#endif  /* (MAC_START_REQUEST_CONFIRM == 1) */

/*
 * Callback function usr_mlme_sync_loss_ind
 *
 * @param LossReason     Reason for synchronization loss.
 * @param PANId          The PAN identifier with which the device lost
 *                       synchronization or to which it was realigned.
 * @param LogicalChannel The logical channel on which the device lost
 *                       synchronization or to which it was realigned.
 * @param ChannelPage    The channel page on which the device lost
 *                       synchronization or to which it was realigned.
 *
 * @return void
 *
 */

void usr_mlme_sync_loss_ind(uint8_t LossReason, uint16_t PANId, uint8_t LogicalChannel, uint8_t ChannelPage)
{
	TRACE_USER("usr_mlme_sync_loss_ind LossReason = %d\n", (int)LossReason);
}

/*
 * @brief Application specific function to assign a short address
 *
 */
static bool assign_new_short_addr(uint64_t addr64, uint16_t *addr16)
{
	uint8_t i;

	/* Check if device has been associated before */
	for (i = 0; i < MAX_NUMBER_OF_DEVICES; i++)
  {
		if (device_list[i].short_addr == 0x0000)
    {
			/* If the short address is 0x0000, it has not been used before */
			continue;
		}

		if (device_list[i].ieee_addr == addr64)
    {
			/* Assign the previously assigned short address again */
			*addr16 = device_list[i].short_addr;
			TRACE_USER("reaasign short addr 0x%08ld, addr = %d\n", addr64, device_list[i].short_addr);
			return true;
		}
	}

	for (i = 0; i < MAX_NUMBER_OF_DEVICES; i++)
  {
		if (device_list[i].short_addr == 0x0000)
    {
			*addr16 = CPU_ENDIAN_TO_LE16(i + 0x0001);
			device_list[i].short_addr = CPU_ENDIAN_TO_LE16(i + 0x0001);   /* get next short address **/
			device_list[i].ieee_addr = addr64;                            /* store extended address */
			no_of_assoc_devices++;
//  	  TRACE_USER("assign_new_short_addr 0x%08lx, addr = %u\n", (uint32_t)addr64, device_list[i].short_addr);
			return true;
		}
	}
	/* If we are here, no short address could be assigned. */
	return false;
}

/*
 * @brief Callback function for initiation of broadcast data transmission
 *
 * @param parameter Pointer to callback parameter
 *                  (not used in this application, but could be used
 *                  to indicated LED to be switched off)
 */
// #ifdef SIO_HUB
// const char Display_Broadcast_Tx_Count[] = "Broadcast frame Tx count:  %lu\r\n";
// #endif
// static void bc_data_cb(void *parameter)
// {
// 	/* Store the current MSDU handle to be used for a broadcast data frame.
// 	**/
// 	static uint8_t curr_msdu_handle_temp;
// 	uint8_t src_addr_mode;
// 	wpan_addr_spec_t dst_addr;
// 	uint8_t payload;
// #ifdef SIO_HUB
// 	/*char sio_array[255];*/
// #endif
//
// 	/*
// 	 * Request transmission of broadcast data to all devices.
// 	 *
// 	 * Since this is a beacon-enabled network,
// 	 * this request will just queue this frame into the broadcast data
// 	 * queue.
// 	 *
// 	 * Once this the next beacon frame is about to be transmitted,
// 	 * the broadcast data frame will be announced by setting
// 	 * the frame pending bit of the frame control field of this particular
// 	 * beacon frame.
// 	 *
// 	 * Immediately after the successful transmission of the beacon frame,
// 	 * the pending broadcast frame will be transmitted.
// 	 */
// 	src_addr_mode = WPAN_ADDRMODE_SHORT;
// 	dst_addr.AddrMode = WPAN_ADDRMODE_SHORT;
// 	dst_addr.PANId = DEFAULT_PAN_ID;
// 	/* Broadcast destination address is used. */
// 	dst_addr.Addr.short_address = BROADCAST;
//
// 	payload = (uint8_t)rand(); /* Any dummy data */
// 	curr_msdu_handle_temp++;     /* Increment handle */
// 	tx_cnt++;
//
// #ifdef SIO_HUB
// 	printf(Display_Broadcast_Tx_Count, tx_cnt);
// #endif
//
// 	/* The transmission is direct, but without acknowledgment. */
// // 	if (wpan_mcps_data_req(src_addr_mode,
// // 			&dst_addr,
// // 			1,     /* One octet */
// // 			&payload,
// // 			curr_msdu_handle_temp,
// // 			WPAN_TXOPT_OFF
// // #if (defined MAC_SECURITY_ZIP) || (defined MAC_SECURITY_2006)
// // 			, 0, NULL, 0, 0
// // #endif
// // 			)
// // 			) {
// // 		/*		LED_On(LED_DATA); */
// // 	} else {
// // 		/*
// // 		 * Data could not be queued into the broadcast queue.
// // 		 * Add error handling if required.
// // 		 */
// // 	}
//
// 	/* Start timer to initiate next broadcast data transmission. */
// // 	sw_timer_start(APP_TIMER_BC_DATA,
// // 			((uint32_t)APP_BC_DATA_DURATION_MS * 1000),
// // 			SW_TIMEOUT_RELATIVE,
// // 			(FUNC_PTR)bc_data_cb,
// // 			NULL);
//
// 	parameter = parameter; /* Keep compiler happy. */
// }

// /*
//  * @brief Callback function for initiation of indirect data transmission
//  *
//  * @param parameter Pointer to callback parameter
//  *                  (not used in this application, but could be used
//  *                  to indicated LED to be switched off)
//  */
// #ifdef SIO_HUB
// const char Display_Queue_Device_Data[] = "Queue data for device %u ";
// const char Display_MSDU_Handle[] = "(MSDU handle:  %u)\r\n";
// #endif
// static void indirect_data_cb(void *parameter)
// {
// 	uint16_t cur_device;
// 	uint8_t src_addr_mode;
// 	wpan_addr_spec_t dst_addr;
// 	const char *payload = "Indirect Data from Coordinator";
//
// //	TRACE_USER("indirect_data_cb\n");
//
// 	/* Loop over all associated devices. */
// 	for (cur_device = 0; cur_device < no_of_assoc_devices; cur_device++) {
// #ifdef SIO_HUB
// 		printf(Display_Queue_Device_Data, (cur_device + 1));
// #endif
//
// 		/*
// 		 * Request transmission of indirect data to device.
// 		 * This will just queue this frame into the indirect data queue.
// 		 * Once this particular device polls for pending data,
// 		 * the frame will be delivered to the device.
// 		 */
// 		src_addr_mode = WPAN_ADDRMODE_SHORT;
// 		dst_addr.AddrMode = WPAN_ADDRMODE_SHORT;
// 		dst_addr.PANId = DEFAULT_PAN_ID;
// 		dst_addr.Addr.short_address = device_list[cur_device].short_addr;
// 		curr_msdu_handle++; /* Increment handle */
//
// #ifdef SIO_HUB
// 		printf(Display_MSDU_Handle, curr_msdu_handle);
// #endif
//
// 		if (!wpan_mcps_data_req(src_addr_mode,
// 				&dst_addr,
// 				strlen(payload),  /* One octet */
// 				(uint8_t *)payload,
// 				curr_msdu_handle,
// 				WPAN_TXOPT_INDIRECT_ACK
// #if (defined MAC_SECURITY_ZIP) || (defined MAC_SECURITY_2006)
// 				, ZIP_SEC_MIN, NULL, ZIP_KEY_ID_MODE,
// 				device_list[cur_device].short_addr
// #endif
// 				)
// 				)
// 		{
// 		}
// 	}
//
// 	/* Start timer to initiate indirect data transmission. */
// 	sw_timer_start(APP_TIMER_INDIRECT_DATA,
// 			((uint32_t)APP_INDIRECT_DATA_DURATION_MS * 1000),
// 			SW_TIMEOUT_RELATIVE,
// 			(FUNC_PTR)indirect_data_cb,
// 			NULL);
//
// 	parameter = parameter; /* Keep compiler happy. */
// }

#ifdef GTS_SUPPORT

/*
 * @brief Callback function for initiation of gts data transmission
 *
 * @param parameter Pointer to callback parameter
 *                  (not used in this application, but could be used
 *                  to indicated LED to be switched off)
 */
static void gts_data_cb(void *parameter)
{
	uint16_t cur_device;
	uint8_t src_addr_mode;
	wpan_addr_spec_t dst_addr;

//	TRACE_USER("gts_data_cb\n");

	/* Loop over all associated devices. */
	for (cur_device = 0; cur_device < no_of_assoc_devices; cur_device++)
  {
//#ifdef SIO_HUB
//		TRACE_USER( "GTS data for device %d\n", (int)cur_device + 1);
//#endif

		/*
		 * Request transmission of indirect data to device.
		 * This will just queue this frame into the indirect data queue.
		 * Once this particular device polls for pending data,
		 * the frame will be delivered to the device.
		 */
		src_addr_mode = WPAN_ADDRMODE_SHORT;
		dst_addr.AddrMode = WPAN_ADDRMODE_SHORT;
		dst_addr.PANId = DEFAULT_PAN_ID;
		dst_addr.Addr.short_address = device_list[cur_device].short_addr;

		/* payload = (uint8_t)rand(); / * Any dummy data * / */
		gts_msdu_handle++; /* Increment handle */

#ifdef SIO_HUB
//		printf(Display_MSDU_Handle, gts_msdu_handle);
#endif
		if (!wpan_mcps_data_req(src_addr_mode, &dst_addr,	sizeof(gts_payload),  /* One octet */	gts_payload, gts_msdu_handle,	WPAN_TXOPT_GTS_ACK
#ifdef MAC_SECURITY_ZIP
				, ZIP_SEC_MIN, NULL, ZIP_KEY_ID_MODE,	device_list[cur_device].short_addr
#endif /*  */
				))
    {
			/*
			 * Data could not be queued into the indirect queue.
			 * Add error handling if required.
			 */
		}
	}

	/* Start timer to initiate indirect data transmission. */
	sw_timer_start(APP_TIMER_GTS_DATA, ((uint32_t)APP_GTS_DATA_DURATION_MS * 1000), SW_TIMEOUT_RELATIVE, (FUNC_PTR)gts_data_cb, NULL);

	parameter = parameter; /* Keep compiler happy. */
}

#endif
