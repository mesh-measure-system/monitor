/*
 * CList.h
 *
 *  Created on: 17-12-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CLIST_H_
#define CLIST_H_

#include "stddef.h"


template <class Titem>
class CList;

template <class Titem>
class CListItem
{
  public:
    Titem*  next_item;
};

template <class Titem>
class CListIterator
{
  private:
    Titem*  first_item;
    Titem*  next_item;
  public:

    CListIterator()
    {
      next_item = NULL;
      first_item = NULL;
    }
    
    CListIterator(CList<Titem>* list)
    {
      next_item = list->first_item;
      first_item = list->first_item;;
    }

    void init(CList<Titem>* list)
    {
      next_item = list->first_item;
      first_item = list->first_item;;
    }
    
    void reset()
    {
      next_item = first_item;
    }
    
    Titem* next()
    {
      if (next_item != NULL)
      {
        next_item = next_item->next_item;
      }
      return next_item;
    }

    Titem* get()
    {
      return next_item;
    }

};

template <class Titem>
class CList
{
  public:
    Titem*  first_item;
    void init()
    {
      first_item = NULL;
    }

    bool add_item(CListItem<Titem>* new_item)
    {
      if (first_item == NULL)
      {
        first_item = (Titem*)new_item;
      }
      else
      {
        CListIterator<Titem> list(this);
        CListItem<Titem>* last_item;

        while(list.get())
        {
          if (list.get() == new_item)
          {
            // cannot add the same command
            return false;
          }
          last_item = list.get();
          list.next();
        }
        last_item->next_item = (Titem*)new_item;
      }
      new_item->next_item = NULL;
      return true;
    }
    virtual ~CList() {};
};

#endif /* CLIST_H_ */
