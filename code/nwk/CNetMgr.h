/*
* CNetMgr.h
*
* Created: 2014-11-21 09:09:30
* Author: Przemek
*/


#ifndef __CNETMGR_H__
#define __CNETMGR_H__

#include <stdint.h>

#include "CRun.h"
#include "CList.h"
#include "avr2025_mac.h"
#include "CNetwork.h"


#define CNET_MGR_MSDU_FIRST 10
#define CNET_MGR_MSDU_LEN   10

typedef struct
{
	wpan_addr_spec_t	SrcAddrSpec;
	wpan_addr_spec_t	DstAddrSpec;
	uint8_t				    mpduLinkQuality;
	uint8_t				    DSN;
	uint8_t				    dstEndPoint;
	uint8_t				    srcEndPoint;
  uint8_t           control;
}cnet_frame_data;



#define CNET_MAX_PAYLOAD_SIZE (aMaxMACPayloadSize - sizeof(cnet_frame_header_t))

class CNetPortReceived : public CListItem<CNetPortReceived>
{
	public:
		uint8_t				port_num;
	public:
	virtual void portData(cnet_frame_data* frame_data_ptr, uint8_t data_len, uint8_t* data_ptr) = 0;
	virtual ~CNetPortReceived() {};
};

class CNetSendConfirm
{
  public:
  public:
    virtual void dataDelivered(uint8_t status, int32_t data_id) = 0;

    virtual ~CNetSendConfirm() {};
};


class CNetMgr : public CRun
{
//variables
public:
protected:
	CList<CNetPortReceived> ports_registered;

  uint8_t           msdu_idx;
  CNetSendConfirm*  msdu_list[CNET_MGR_MSDU_LEN];
  int32_t           msdu_data_id[CNET_MGR_MSDU_LEN];
private:

//functions
public:
    void Init();
    virtual void run();
    virtual bool isIdle() {return true;};

	  bool SendData(CNetSendConfirm* confirm_p, int32_t data_id, uint16_t short_addr, uint8_t dstEndPoint, uint8_t srcEndPoint, uint8_t ctrl, uint8_t data_len, uint8_t* data_ptr);
    void confirmDelivery(uint8_t msduHandle, uint8_t status);

	  bool RegisterPort(uint8_t port_num, CNetPortReceived* port_recived);

	  bool ParseFrame(wpan_addr_spec_t *SrcAddrSpec, wpan_addr_spec_t *DstAddrSpec,	uint8_t msduLength,	uint8_t *msdu, uint8_t mpduLinkQuality,	uint8_t DSN);

    void ParseBeacon();

    virtual const char* getClassName() {return __PRETTY_FUNCTION__;}


	~CNetMgr() {};
protected:

private:
//	CNetMgr( const CNetMgr &c );
//	CNetMgr& operator=( const CNetMgr &c );

}; //CNetMgr

extern CNetMgr netMgr;

#endif //__CNETMGR_H__
