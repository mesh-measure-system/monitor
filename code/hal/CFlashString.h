/*
 * CFlashString.h
 *
 *  Created on: 29-12-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CFLASHSTRING_H_
#define CFLASHSTRING_H_

#include "avr/pgmspace.h"

#ifdef __cplusplus
	extern "C" {
#endif

// When compiling programs with this class, the following gcc parameters
// dramatically increase performance and memory (RAM) efficiency, typically
// with little or no increase in code size.
//     -felide-constructors
//     -std=c++0x

#ifdef __cplusplus
class __FlashStringHelper;
#define F(string_literal) (reinterpret_cast<const __FlashStringHelper *>(PSTR(string_literal)))
#else
#define F(string_literal) ((PSTR(string_literal)))
#endif
//
//class CFlashString
//{
//  public:
//    virtual ~CFlashString();
//};

#ifdef __cplusplus
}
#endif

#endif /* CFLASHSTRING_H_ */
