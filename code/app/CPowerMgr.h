/*
 * CPowerMgr.h
 *
 *  Created on: 07-11-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CPOWERMGR_H_
#define CPOWERMGR_H_

#include "CRun.h"

class CPowerMgr : public CRun
{
	private:

	public:
		void Init();
		virtual void run();
		virtual bool isIdle();
};

extern CPowerMgr powerMgr;

#endif /* CPOWERMGR_H_ */
