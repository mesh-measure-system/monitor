/*
 * CFatSDMain.cpp
 *
 *  Created on: 9 maj 2014
 *      Author: Przemek
 */

#include "CFatSDMain.h"
#include <limits.h>

#include <string.h>

#include "diskio.h"
#include "ff.h"
#include "halT2RTC.h"
#include "CCommandsMgr.h"
#include "CArgParser.h"

#include "sysclk.h"

#include "trace.h"


CFatSDMain fatSDMgr;

FATFS MemoryFatFs;    /* FatFs work area needed for each volume */
FATFS uSDFatFs;       /* FatFs work area needed for each volume */


class CCmdFormat : public CCommand
{
	public:
		virtual bool start_command(CStream* stream, char* argv[], int argc)
		{
			UNUSED(stream);
			UNUSED(argv);
			UNUSED(argc);

      FRESULT res;

      /* Create FAT volume with default cluster size and SFD type */
      res = f_mkfs("0:", 1, 0);
      TRACE_USER("f_mkfs result = %d\n", (int)res);
		  return false;
		};
		virtual bool is_idle() {return true;};
		virtual bool is_finished() {return true;};

		virtual void print_help(CStream* stream) {stream->printf("Formats Memory\n");};

		virtual ~CCmdFormat() {};
};

CCmdFormat cmdFormat;

/*********************************************************************************************************************/
/*
 *
 */
class CLSCmd: public CCommand
{
  public:
  bool cmd_running;
  bool long_list;

  DIR dir;
  char *fn;   /* This function assumes non-Unicode configuration */

  virtual bool start_command(CStream* stream, char* argv[], int argc)
  {
    CArgParser  argP(argv, argc, F("l"));
    int         c;
    bool        finish = false;
    FRESULT     res;
    
    // initialize
    long_list   = false;
    
    while (!finish)
    {
      c = argP.getSwitch();
      switch (c)
      {
        case 'l':
        {
          long_list = true;
          break;
        }
        case CArgParser::CARG_INVALID_ARG:
        default:
        {
          break;
        }
        case CArgParser::CARG_END_OF_SWITCH:
        {
          finish = true;
          break;
        }
      }
    }

    int idx = 0;
    while (argP.not_switch_idx > idx)
    {
      FILINFO     fno;
      char        lfn[_MAX_LFN + 1];   /* Buffer to store the LFN */

      lfn[0]      = 0;
      fno.lfname  = lfn;
      fno.lfsize  = sizeof(lfn);
      // check if this can be a file
      res = f_stat(argv[idx], &fno);
      if (res == FR_OK)
      {
        if (fno.fattrib & AM_DIR)
        {
          // directory 
          res = f_opendir(&dir, argv[idx]);                       /* Open the directory */
          if (res == FR_OK)
          {
            cmd_running = true;
            stream->printf(F("Dir %s:\n"), argv[idx]);
          }
          else
          {
            stream->printf(F("Problem with open %s:%d\n"), argv[idx], res);
          }
        }
        else
        {
          // file
          print_file_attr(stream, &fno);
        }
      }
      else
      {
        stream->printf(F("Problem with open %s:%d\n"), argv[idx], res);
      }
      idx++;
    }

    if (!cmd_running)
    {
      stream->printf(F("End\n"));
    }
    return cmd_running;
  };
  
  void print_file_attr(CStream* stream, FILINFO* fno_ptr)
  {
    if (fno_ptr->fname[0] != '.') /* Ignore dot entry */
    {
#if _USE_LFN
      fn = *(fno_ptr->lfname) ? fno_ptr->lfname : fno_ptr->fname;
#else
      fn = fno_ptr->fname;
#endif
      if (fno_ptr->fattrib & AM_DIR)
      {                    /* It is a directory */
        stream->printf(F("D %s\n"), fn);
      }
      else
      {                   /* It is a file. */
        if (long_list)
        {
          stream->printf(F("F %s %lu %u/%02u/%02u, %02u:%02u:%02u\n"), fn, fno_ptr->fsize,
                    (fno_ptr->fdate >> 9) + 1980, fno_ptr->fdate >> 5 & 15, fno_ptr->fdate & 31,
                    fno_ptr->ftime >> 11, fno_ptr->ftime >> 5 & 63, (fno_ptr->ftime & 31) * 2);
        }
        else
        {
          stream->printf(F("F %s\n"), fn);
        }
      }
    }   
  }
  virtual bool run_command(CStream* stream)
  {
    FRESULT     res;
    FILINFO     fno;
    char        lfn[_MAX_LFN + 1];   /* Buffer to store the LFN */

    lfn[0]      = 0;
    fno.lfname  = lfn;
    fno.lfsize  = sizeof(lfn);

    if (stream->isOutBuffEmpty() == false)
    {
      // wait for empty buffer
      return true;
    }
    
    res = f_readdir(&dir, &fno);                   /* Read a directory item */
    if (res == FR_OK)
    {
      // wd reset - just in case of the long operation
      wdt_reset();
      if (fno.fname[0] == 0)
      {
        // end of dir
        f_closedir(&dir);
        stream->printf(F("End\n"));
        cmd_running = false;
      }
      else
      {
        print_file_attr(stream, &fno);
      }
    }
    return cmd_running;
  }
  virtual bool break_command(CStream* stream)
  {
    f_closedir(&dir);
    cmd_running = false;
    stream->printf(F("Break\n"));
    return false;
  }
  virtual bool is_idle() {return !cmd_running;};
  virtual bool is_finished() {return !cmd_running;};
  virtual void print_help(CStream* stream) {stream->printf("Lists files [-l] dir\n");};
  virtual ~CLSCmd() {};
};

CLSCmd cmdLS;

/*********************************************************************************************************************/
/*
 *
 */
class CRMCmd: public CCommand
{
  public:

  virtual bool start_command(CStream* stream, char* argv[], int argc)
  {
    CArgParser  argP(argv, argc, F(""));
    int         c;
    bool        finish = false;
    FRESULT     res;
    char        path[_MAX_LFN + 1];

    while (!finish)
    {
      c = argP.getSwitch();
      switch (c)
      {
        case CArgParser::CARG_INVALID_ARG:
        default:
        {
          break;
        }
        case CArgParser::CARG_END_OF_SWITCH:
        {
          finish = true;
          break;
        }
      }
    }

    int idx = 0;
    while ((argP.not_switch_idx > idx))
    {
      // file name
      strncpy(path, argv[idx], sizeof(path));
      res = f_unlink(path);             /* remove file or directory*/
      if (res == FR_OK)
      {
        stream->printf(F("Removed %s\n"), path);
      }
      else
      {
        stream->printf(F("Remove error %d\n"), res);
      }
      idx++;
    }      

    return true;
  };
  virtual bool break_command(CStream* stream){return false;}
  virtual bool is_idle() {return true;};
  virtual bool is_finished() {return true;};
  virtual void print_help(CStream* stream) {stream->printf("remove file/dir \"file name\"\n");};
  virtual ~CRMCmd() {};
};

CRMCmd cmdRM;

/*********************************************************************************************************************/
/*
 *
 */
class CCatCmd: public CCommand
{
  public:
  bool cmd_running;

  uint32_t  start_idx;
  uint32_t  end_idx;
  int32_t   current_idx;
  
  FIL       file;
  
  virtual bool start_command(CStream* stream, char* argv[], int argc)
  {
    CArgParser  argP(argv, argc, F("s:e:"));
    int         c;
    bool        finish = false;
    FRESULT     res;
    
    // initialize
    start_idx   = 0;
    end_idx     = ULONG_MAX;
    current_idx = ULONG_MAX;  
    cmd_running = false;
    
    while (!finish)
    {
      c = argP.getSwitch();
      switch (c)
      {
        case 's':
        {
          start_idx = argP.getSwitchArgumentLong();
          break;
        }
        case 'e':
        {
          end_idx = argP.getSwitchArgumentLong();
          break;
        }
        case CArgParser::CARG_INVALID_ARG:
        default:
        {
          break;
        }
        case CArgParser::CARG_END_OF_SWITCH:
        {
          finish = true;
          break;
        }
      }
    }

    if ((argP.not_switch_idx == 1))
    {
      bool push_end = false;
      // file name
      TRACE_USER("File: '%s', -s=%ld, -e=%ld\n", argv[0], start_idx, end_idx);
      if (end_idx != ULONG_MAX)
      {
        if (end_idx < start_idx)
        {
          // params problem
          stream->printf(F("End idx problem\n"));
          push_end = true;
        }
      }
      if (push_end == false)
      {
        res = f_open(&file, argv[0], FA_READ);
        if (res == FR_OK)
        {
          // file opened successfully
          cmd_running = true;
          if (start_idx > 0)
          {
            f_lseek(&file, start_idx);
            if (f_tell(&file) == (unsigned)start_idx)
            {
              // move start position successful
              // nothing to do
            }
            else
            {
              // problem with move start position
              cmd_running = false;
              stream->printf(F("Start position problem\n"));
            }
          }
        }
        else
        {
          stream->printf(F("Problem with open file\n"));
        }
      }
    }      

    if (!cmd_running)
    {
      stream->printf(F("End\n"));
    }
    stream->flushStream();
    return cmd_running;
  };

  virtual bool run_command(CStream* stream)
  {
    FRESULT res;
    
    int16_t   to_send = 50;//stream->getRemainingOutBuffSize();
    char*     buff = stream->getPrintfBuffer();
    uint16_t  read_bytes;
    int32_t   read_end;
    int16_t   to_read;

    if (!stream->isOutBuffEmpty())
    {
      // out buff is not empty
      return cmd_running;
    }
    
    // is end of the file
    if ((f_tell(&file) == f_size(&file)) ||
        (f_tell(&file) == end_idx))
    {
      // end of the file
      f_close(&file);
      cmd_running = false;
      stream->printf(F("End\n"));
    }
    else
    {
      if (end_idx != ULONG_MAX)
      {
        read_end = end_idx;
      }
      else
      {
        read_end = f_size(&file);
      }
      read_end -= f_tell(&file);
    
      if (to_send < read_end)
      {
        to_read = to_send;
      }
      else
      {
        to_read = read_end;
      }

    
      res = f_read(&file, buff, to_read, &read_bytes);
      if ((res == FR_OK)/* &&
          (read_bytes > 0)*/)
      {
        stream->write(buff, read_bytes);
      }
      else
      {
        // end of read
        f_close(&file);
        cmd_running = false;
        stream->printf(F("End\n"));
      }
    }
    stream->flushStream();

    return cmd_running;
  }
  virtual bool break_command(CStream* stream)
  {
    f_close(&file);
    stream->printf(F("Break\n"));
    cmd_running = false;
    stream->flushStream();
    return cmd_running;
  }
  virtual bool is_idle() {return !cmd_running;};
  virtual bool is_finished() {return !cmd_running;};
  virtual void print_help(CStream* stream) {stream->printf("Displays file content [-s=start byte] [-e=end byte] file_name\n");};
  virtual ~CCatCmd() {};
};

CCatCmd cmdCat;

/*********************************************************************************************************************/
/*
 *
 */
void CFatSDMain::Init()
{
  FRESULT res;

  sysclk_enable_peripheral_clock(&SPCR);

  // Initialize pins for SDCARD
  HAL_GPIO_SPI_nSDCS_out();
  HAL_GPIO_SPI_nSDCS_set();
  HAL_GPIO_CARD_INSERT_in();
  HAL_GPIO_CARD_INSERT_pullup();

  // Initialize pins for AT45DB161D memory

  HAL_GPIO_SPI_nCS_out();
  HAL_GPIO_SPI_nCS_set();

  HAL_GPIO_SPI_MISO_in();
  HAL_GPIO_SPI_MOSI_out();
  HAL_GPIO_SPI_SCK_out();


  //mounting filesystems
  res = f_mount(&MemoryFatFs, "0:", 0);
  TRACE_USER("Memory mount result = %d\n", (int)res);

  res = f_mount(&uSDFatFs, "1:", 0);
  TRACE_USER("uSD mount result = %d\n", (int)res);

  {
    FATFS *fs = &uSDFatFs;
    DWORD fre_clust, fre_sect, tot_sect;

  	wdt_reset();
    /* Get volume information and free clusters of drive 1 */
    res = f_getfree("1:", &fre_clust, &fs);

	  wdt_reset();
    if (res == FR_OK)
    {
      /* Get total sectors and free sectors */
      tot_sect = (fs->n_fatent - 2) * fs->csize;
      fre_sect = fre_clust * fs->csize;

      /* Print the free space (assuming 512 bytes/sector) */
      TRACE_USER("%10lu KiB total drive space.\n", tot_sect / 2);
      TRACE_USER("%10lu KiB available.\n", fre_sect / 2);
    }
    else
    {
      TRACE_USER("SD card not mounted\n");
    }
  }

  {
    FATFS *fs = &MemoryFatFs;
    DWORD fre_clust, fre_sect, tot_sect;

  	wdt_reset();
    /* Get volume information and free clusters of drive 0 */
    res = f_getfree("0:", &fre_clust, &fs);

	  wdt_reset();
    if (res == FR_OK)
    {
      /* Get total sectors and free sectors */
      tot_sect = (fs->n_fatent - 2) * fs->csize;
      fre_sect = fre_clust * fs->csize;

      /* Print the free space (assuming 512 bytes/sector) */
      TRACE_USER("%10lu KiB total drive space.\n", tot_sect / 2);
      TRACE_USER("%10lu KiB available.\n", fre_sect / 2);
    }
    else
    {
      TRACE_USER("MEM flash not mounted\n");
    }
  }
}

void CFatSDMain::InitCommands()
{
 	cmdFormat.init((char*)"format");
  cmdLS.init((char*)"ls");
  cmdRM.init((char*)"rm");
  cmdCat.init((char*)"cat");
}
