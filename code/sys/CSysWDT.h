/*
* CSysWDT.h
*
* Created: 2014-11-24 20:22:10
* Author: Przemek
*/


#ifndef __CSYSWDT_H__
#define __CSYSWDT_H__

#include "CRun.h"

class CSysWDT: public CRun
{
//variables
public:
protected:
private:

//functions
public:
	void Init();

	void Reboot(int wait_to_reboot_ms = 200);

    virtual void run();
    virtual bool isIdle();

	CSysWDT();
	~CSysWDT() {};
protected:
private:
// 	CSysWDT( const CSysWDT &c );
// 	CSysWDT& operator=( const CSysWDT &c );

}; //CSysWDT

extern CSysWDT sysWDT;
#endif //__CSYSWDT_H__
