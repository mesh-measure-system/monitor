/*
 * CPowerMgr.cpp
 *
 *  Created on: 07-11-2013
 *      Author: Przemek Kieszkowski
 */

#include <stdlib.h>

#include "CPowerMgr.h"
#include "CApp.h"
#include "trace.h"
#include "sysTimer.h"

// class instance
CPowerMgr powerMgr;

void CPowerMgr::Init()
{
	CRun::Init();
}

extern  volatile bool txRunning;

void CPowerMgr::run()
{
//FIXME: [Adam] ??
#if 0
	timeval curr_time;
	{
		bool idle = true;
		// check if all registered modules are in the idle state
		CListIterator<CRun> run_iter(&app.run_classes);

		while(run_iter.get())
		{
			idle = idle && run_iter.get()->isIdle();
			run_iter.next();
		}

		if (idle)
		{
			uint32_t  nextFire;
			// all classes are in idle - we can go to low power
			// check timer for length of the low power state

			nextFire = SYS_TimerGetNextFire();

			if (nextFire != 0)
			{
				TRACE_USER("Next fire %ld\n", nextFire);
				// wait while uart finishes
				while(isTxRunning()) {};
				HAL_Sleep(SYS_TimerGetNextFire());
			}
		}
	}
#endif
}

bool CPowerMgr::isIdle()
{
  return true;
}
