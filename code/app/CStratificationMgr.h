/*
 * CStratificationMgr.h
 *
 *  Created on: 16-11-2013
 *      Author: Przemek Kieszkowski
 */

#include "CADC.h"
#include "CPin.h"
#include "sysTimer.h"
#include "CSettingsMgr.h"


#ifndef CSTRATIFICATIONMGR_H_
#define CSTRATIFICATIONMGR_H_

#define EXTENDED_DEBUG 0

class CStratification : public CADCdata
{
	public:
		CPin      adc_power;
		double    data;
		void Init(ADCChannel_t chann, HAL_AdcVoltageReference_t vref);

		// function is called before start conversion on the selected channel
		// it's purpose is to setup peripheral (enable vcc and simillar)
		virtual void adcSetupChannel();

		virtual void conversionFinished();

		// returns stratification in the mm resolution
		double getStratification();
};

class CStratificationMgr : public CRun, public CTimerFired
{
	private:
		SYS_Timer_t timer;
		SYS_Timer_t pow_en_timer;
		CSetting<uint16_t, SETTING_UINT16> power_enable_time;
		CSetting<uint32_t, SETTING_UINT32> fast_measure_sleep;
		CSetting<uint32_t, SETTING_UINT32> fast_measure_period;
		CSetting<uint32_t, SETTING_UINT32> slow_measure_sleep;

	public:
		enum strState_t {IDLE, ENABLE_SENSOR_POWER, START_MEASURE, MEASURED};
		strState_t state;
		CStratification stratification;

		void Init();
		void setState(strState_t new_state);

		virtual void run();
		virtual bool isIdle();
		virtual void timerFired(struct SYS_Timer_t* timer_fired);
		virtual ~CStratificationMgr() {};
};

extern CStratificationMgr stratificationMgr;

#endif /* CSTRATIFICATIONMGR_H_ */
