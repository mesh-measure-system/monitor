/*
 * CLedMgr.cpp
 *
 *  Created on: 01-12-2013
 *      Author: Przemek Kieszkowski
 */

#include <compiler.h>
#include "CLedMgr.h"
#include <stdio.h>


CLedMgr ledMgr;

static uint16_t pow_sq[LEDMGR_MAX_SEQUENCE_LEN] = {100, 59900, 0, 0, 0, 0};
static uint16_t alarm_sq[LEDMGR_MAX_SEQUENCE_LEN] = {100, 900, 100, 900, 100, 3900};

void CLedSequence::Init(char* seq_name, uint16_t* seq_init, uint32_t seqence_len_ms)
{
	snprintf(this->seq_name, sizeof(this->seq_name), "%s_seq", seq_name);
	snprintf(this->seq_len_name, sizeof(seq_len_name), "%s_len", seq_name);

	sequence.Init(this->seq_name, seq_init);
	len.Init(this->seq_len_name, seqence_len_ms);
}

void CLedPlay::timerFired(struct SYS_Timer_t* timer)
{
	if (timer == &play_timer)
	{
		play_handler(timer);
	}
	else if (timer == &stop_timer)
	{
		stop_handler(timer);
	}
}

void CLedPlay::play_handler(SYS_Timer_t *timer)
{
	UNUSED(timer);
	next_sequence_idx++;

	if ((next_sequence_idx == LEDMGR_MAX_SEQUENCE_LEN) ||
	(seqence->sequence.getItem(next_sequence_idx) == 0))
	{
		// reset play sequence
		next_sequence_idx = 0;
	}

	Set(!(next_sequence_idx & 0x1));
	play_timer.interval = seqence->sequence.getItem(next_sequence_idx);
	SYS_TimerStart(&play_timer, this);
}

void CLedPlay::stop_handler(SYS_Timer_t *timer)
{
	UNUSED(timer);
	Stop();
}

void CLedPlay::Stop()
{
	next_sequence_idx = 0;
	SYS_TimerStop(&play_timer);
	SYS_TimerStop(&stop_timer);
}

void CLedPlay::Play(CLedSequence* seq_p)
{
	Stop();
	next_sequence_idx = 0;
	// enable led
	Set(true);

	seqence = seq_p;

	play_timer.mode = SYS_TIMER_INTERVAL_MODE;
	play_timer.interval = seqence->sequence.getItem(next_sequence_idx);
	SYS_TimerStart(&play_timer, this);

	stop_timer.mode = SYS_TIMER_INTERVAL_MODE;
	stop_timer.interval = seqence->len.get();
	if (stop_timer.interval != 0)
	{
		SYS_TimerStart(&stop_timer, this);
	}
}

void CLedMgr::Init()
{
	CRun::Init();

	pinBlue.Init(CPin::PIN_PORTE, 5, CPin::PIN_OUT);
	ledBlue.Init(&pinBlue, false);

	pinGreen.Init(CPin::PIN_PORTE, 4, CPin::PIN_OUT);
	ledGreen.Init(&pinGreen, false);

	pinRed.Init(CPin::PIN_PORTE, 3, CPin::PIN_OUT);
	ledRed.Init(&pinRed, false);
	
	pinAlarm.Init(CPin::PIN_PORTB, 7, CPin::PIN_OUT);
	ledAlarm.Init(&pinAlarm, false);

	powerSq.Init((char*)"power", pow_sq, 0L);
	alarmSq.Init((char*)"alarm", alarm_sq, 60*1000L);

	ledGreen.Play(&powerSq);
}

void CLedMgr::run()
{

}

bool CLedMgr::isIdle()
{
	return true;
}


