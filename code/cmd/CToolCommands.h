/*
 * CToolCommands.h
 *
 *  Created on: 10 lut 2014
 *      Author: Przemek Kieszkowski
 */

#ifndef CTOOLCOMMANDS_H_
#define CTOOLCOMMANDS_H_

class CToolCommands
{
  public:
    void Init(void);
    virtual ~CToolCommands() {};
};

extern CToolCommands toolCmd;


#endif /* CTOOLCOMMANDS_H_ */
