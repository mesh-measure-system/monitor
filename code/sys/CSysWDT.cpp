/*
* CSysWDT.cpp
*
* Created: 2014-11-24 20:22:09
* Author: Przemek
*/


#include "CSysWDT.h"

#include <avr/wdt.h>
#include "halWDT.h"
#include "CCommandsMgr.h"
#include "halUart.h"

#include "trace.h"

CSysWDT sysWDT;

class CCmdReboot : public CCommand
{
	public:
		virtual bool start_command(CStream* stream, char* argv[], int argc)
		{
			UNUSED(stream);
			UNUSED(argv);
			UNUSED(argc);

			sysWDT.Reboot();

		return false;
		};
		virtual bool is_idle() {return true;};
		virtual bool is_finished() {return true;};

		virtual void print_help(CStream* stream) {stream->printf("Reboots device\n");};

		virtual ~CCmdReboot() {};
};

CCmdReboot cmdRebot;



// default constructor
CSysWDT::CSysWDT()
{
} //CSysWDT

void CSysWDT::Init()
{
	CRun::Init();

	if (reset_cause & CHIP_RESET_CAUSE_EXTRST)
	{
		// External reset cause
		TRACE_USER("Reset cause: External reset\n");
	}
	if (reset_cause & CHIP_RESET_CAUSE_BOD_CPU)
	{
		// brown-out detected reset cause, same as for CPU
		TRACE_USER("Reset cause: brown-out detected reset\n");
	}
	if (reset_cause & CHIP_RESET_CAUSE_POR)
	{
		// Power-on-reset reset cause
		TRACE_USER("Reset cause: Power-on-reset reset\n");
	}
	if (reset_cause & CHIP_RESET_CAUSE_WDT)
	{
		// Watchdog timeout reset cause
		TRACE_USER("Reset cause: Watchdog timeout reset\n");
	}
	if (reset_cause & CHIP_RESET_CAUSE_JTAG)
	{
		// Software reset reset cause
		TRACE_USER("Reset cause: Software reset reset (JTAG)\n");
	}
	cmdRebot.init((char*)"reboot");
}

void CSysWDT::Reboot(int wait_to_reboot_ms)
{
	// TODO call registered handlers before reboot
//	_delay_ms(wait_to_reboot_ms);
	TRACE_USER("Perform software reset\n\n\n");
	// flush uart
	HAL_Uart0WriteFlush();
	do
	{
		wdt_enable(WDTO_15MS);
		for (;;)
		{
		}
	} while (1);
}


void CSysWDT::run()
{
	// system is working - so tick WDTimer
	wdt_reset();
}
bool CSysWDT::isIdle()
{
	return true;
}
