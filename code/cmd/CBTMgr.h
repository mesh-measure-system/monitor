/* 
* CBTMgr.h
*
* Created: 2015-03-01 18:35:00
* Author: przemek
*/


#ifndef __CBTMGR_H__
#define __CBTMGR_H__

#include "CRun.h"
#include "CSettingsMgr.h"
#include "sysTimer.h"
#include "CStream.h"
#include "halUart.h"
#include "CPin.h"

#define CBT_PRINTF_BUFF_SIZE  HAL_UART_TX_FIFO_SIZE
#define CBT_CMD_MAX_CMD_LEN   HAL_UART_TX_FIFO_SIZE

typedef enum
{
  CBT_MODE_INIT,
  CBT_MODE_COMMAND,
  CBT_MODE_TRANSMIT,
}CBT_Mode_t;

class CBTMgr : public CRun, public CStream, public CTimerFired
{
//variables
public:
  
protected:
private:
    CBT_Mode_t mode;
    
    volatile bool bytes_received;
    char    print_buff[CBT_PRINTF_BUFF_SIZE];
    // buffer where out stream stores data before send it in one shot
    char    out_buff[CBT_CMD_MAX_CMD_LEN];
    uint8_t out_buff_len;
    
    char    cmd[CBT_CMD_MAX_CMD_LEN];
    uint8_t cmd_len;

    void printPrompt();
    void printErrorMessage();

public:
    CPin pinReset;
    CPin pinRTS;
    CPin pinCTS;

  //functions
  public:
    void Init();
    virtual void run();
    virtual bool isIdle();

    virtual char* getPrintfBuffer();
    virtual int   getPrintfBufferSize();
    virtual void  write(char c);

    virtual uint16_t getRemainingOutBuffSize();
    virtual bool isOutBuffEmpty();
    virtual bool flushStream();

    void sendATCommand(char* at_command);

    void uartBytesReceived(uint16_t bytes);

    // for eclipse happiness
	  ~CBTMgr() {};

    virtual void timerFired(struct SYS_Timer_t* timer);

    friend void HAL_Uart1BytesReceived(uint16_t bytes);

}; //CBTMgr

extern CBTMgr BTMgr;

#endif //__CBTMGR_H__
