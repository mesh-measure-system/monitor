/*
 * CLed.cpp
 *
 *  Created on: 02-12-2013
 *      Author: Przemek Kieszkowski
 */

#include "CLed.h"


void CLed::Init(CPin* pin_p, bool on_state)
{
  this->off_state = ~on_state;
  pin = pin_p;
  pin->Out();
  Set(false);
}

void CLed::Set(bool on)
{
  pin->Set(off_state ^ on);
}

void CLed::Toggle()
{
  pin->Toggle();
}
