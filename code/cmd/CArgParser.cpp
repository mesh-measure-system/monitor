/*
 * CArgParser.cpp
 *
 *  Created on: 28-12-2013
 *      Author: Przemek Kieszkowski
 */

#include "CArgParser.h"
#include "string.h"
#include <stdlib.h>


#include "trace.h"

CArgParser::CArgParser(char* argv[], int argc, const __FlashStringHelper* args)
{
  curr_switch = 0;
  argc_c = argc;
  argv_c = argv;
  current_sw_arg_p = NULL;
  not_switch_idx = 0;

  strncpy_P((char*)args_c, (const char*)args, sizeof(args_c) - 1);
  args_c[CARG_MAX_ARGS_LEN - 1] = 0;

  curr_argc = 1;
  curr_argc_idx = 0;

//  TRACE_USER("Command '%s', argc = %d, args = %S\n", argv_c[0], argc, args);
//  if (argc > 1)
//  {
//    for (int ii = 1; ii < argc; ii++)
//    {
//      TRACE_USER("    argument[%d] = '%s'\n", ii, argv_c[ii]);
//    }
//  }
}

int CArgParser::parseSwitch(char sw)
{
//  args_c
  char* occur = strchr((const char*)args_c, sw);
  if (occur != NULL)
  {
    if (occur[1] == ':')
    {
      // option with argument
      return CARG_SWITCH_WITH_ARGUMENT;
    }
    else
    {
      return CARG_SWITCH;
    }
  }
  return CARG_SWITCH_INVALID;
}

char* CArgParser::getSwitchArgument()
{
  return current_sw_arg_p;
}

long int CArgParser::getSwitchArgumentLong()
{
  return strtol(current_sw_arg_p, NULL, 0);
}


int CArgParser::getSwitch()
{
  current_sw_arg_p = NULL;
  while (curr_argc < argc_c)
  {
    while (curr_argc_idx < (int)strlen(argv_c[curr_argc]))
    {
      if (argv_c[curr_argc][0] == CARG_SWITCH_SIGN)
      {
        // switch case
        if (curr_argc_idx > 0)
        {
          if (curr_argc_idx < (int)strlen(argv_c[curr_argc]))
          {
            int ret = argv_c[curr_argc][curr_argc_idx];
            int sw_code;
            sw_code = parseSwitch(ret);
            curr_argc_idx++;
            if (sw_code == CARG_SWITCH)
            {
              return ret;
            }
            else if (sw_code == CARG_SWITCH_WITH_ARGUMENT)
            {
              // parse argument
              switch (argv_c[curr_argc][curr_argc_idx])
              {
                case ':':
                case '=':
                {
                  // consume char - next
                  curr_argc_idx++;
                  current_sw_arg_p = &argv_c[curr_argc][curr_argc_idx];
                  curr_argc++;
                  curr_argc_idx = 0;
                  break;
                }
                case '\0':
                {
                  // param is in the next argument
                  curr_argc++;
                  curr_argc_idx = 0;
                  if (curr_argc < argc_c)
                  {
                    current_sw_arg_p = &argv_c[curr_argc][curr_argc_idx];
                    curr_argc++;
                  }
                  break;
                }
                default:
                {
                  current_sw_arg_p = &argv_c[curr_argc][curr_argc_idx];
                  curr_argc++;
                  curr_argc_idx = 0;
                  break;
                }
              }
              if (current_sw_arg_p == NULL)
              {
                // return error - no argument found
                return CARG_NO_ARG_PARAM_FOUND;
              }
              else
              {
                return ret;
              }
            }
            else if (sw_code == CARG_SWITCH_INVALID)
            {
              return CARG_INVALID_ARG;
            }
          }
          else
          {
            // next param
            break;
          }
        }
        curr_argc_idx++;
      }
      else
      {
        // next param
        argv_c[not_switch_idx] = argv_c[curr_argc];
        not_switch_idx++;
        break;
      }
    }
    curr_argc++;
    curr_argc_idx = 0;
  }
  return CARG_END_OF_SWITCH;
}
