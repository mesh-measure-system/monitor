/*
 * CNotifyMgr.h
 *
 *  Created on: 09-12-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CNOTIFYMGR_H_
#define CNOTIFYMGR_H_

#include "CRun.h"

class CNotifyMgr : public CRun
{
	public:
		virtual ~CNotifyMgr() {};
};

#endif /* CNOTIFYMGR_H_ */
