/*
 * halEeprom.c
 *
 *  Created on: 31-07-2013
 *      Author: Przemek Kieszkowski
 */



#include "hal.h"
#include "halEeprom.h"

#if defined(TRACE_LEVEL)
#undef 	TRACE_LEVEL
#define TRACE_LEVEL TRACE_LEVEL_WARNING
#endif
#include "trace.h"


void      HAL_EepromInit(void)
{
  // nothing to do
}

uint16_t  HAL_EepromGetSize(void)
{
  return HAL_EEPROM_SIZE;
}

void HAL_EepromWriteBytes(uint16_t address, uint8_t* data_p, uint16_t data_len)
{
  while(data_len)
  {
    HAL_EepromWrite(address, *data_p);
    address++;
    data_p++;
    data_len--;
  }
}
void HAL_EepromReadBytes(uint16_t address, uint8_t* data_p, uint16_t data_len)
{
  while(data_len)
  {
    *data_p = HAL_EepromRead(address);
    address++;
    data_p++;
    data_len--;
  }
}

void HAL_EepromWrite(uint16_t address, uint8_t data)
{
	TRACE_USER("HAL_EepromWrite address = %d data = %d\n", (int)address, (int)data);
  if (address > HAL_EEPROM_SIZE)
  {
    return;
  }

  /* Wait for completion of previous write */
  while(EECR & (1<<EEPE))
  {
  	;
  }
  /* Set up address and Data Registers */
  EEAR = address;
  EEDR = data;
  /* Write logical one to EEMPE */
  EECR |= (1<<EEMPE);
  /* Start eeprom write by setting EEPE */
  EECR |= (1<<EEPE);
}

uint8_t   HAL_EepromRead(uint16_t address)
{
	uint8_t ret_value;
  if (address > HAL_EEPROM_SIZE)
  {
    return 0;
  }
  /* Wait for completion of previous write */
  while(EECR & (1<<EEPE))
  {
  	;
  }

  /* Set up address register */
  EEAR = address;
  /* Start eeprom read by writing EERE */
  EECR |= (1<<EERE);
  /* Return data from Data Register */
  ret_value = EEDR;
  
	TRACE_USER("HAL_EepromRead address = %d data = %d\n", (int)address, (int)ret_value);

  return ret_value;
}
