/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2013        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various existing      */
/* storage control module to the FatFs module with a defined API.        */
/*-----------------------------------------------------------------------*/

#include "diskio.h"		/* FatFs lower layer API */
#include "mmc_avr.h"
#include "time.h"

#ifdef TRACE_LEVEL
#undef TRACE_LEVEL
#endif // TRACE_LEVEL

#define TRACE_LEVEL TRACE_LEVEL_WARNING
#include "trace.h"

/*-----------------------------------------------------------------------*/
/* Initialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize(BYTE pdrv /* Physical drive nmuber (0..) */)
{
  DSTATUS result = STA_NOINIT;

  TRACE_USER("disk init id=%d\n", (int) pdrv);
  switch (pdrv)
  {
    case SPI:
    {
//      result = SPI_disk_initialize();

      // translate the reslut code here

      break;
    }
    case MMC:
    {
      result = MMC_disk_initialize();

      // translate the reslut code here

      break;
    }
  }
  return result;
}

/*-----------------------------------------------------------------------*/
/* Get Disk Status                                                       */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status(BYTE pdrv /* Physical drive number (0..) */
)
{
  DSTATUS result = STA_NOINIT;

  switch (pdrv)
  {
    case SPI:
    {
//      result = SPI_disk_status();

      // translate the reslut code here

      return result;
    }
    case MMC:
    {
      result = MMC_disk_status();

      // translate the reslut code here

      return result;
    }
  }
  return STA_NOINIT;
}

/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read(BYTE pdrv, /* Physical drive nmuber (0..) */
    BYTE *buff, /* Data buffer to store read data */
    DWORD sector, /* Sector address (LBA) */
    UINT count /* Number of sectors to read (1..128) */
    )
{
  DRESULT result = RES_PARERR;

  switch (pdrv)
  {
    case SPI:
    {
      // translate the arguments here

//      result = SPI_disk_read(buff, sector, count);

      // translate the reslut code here

      return result;
    }
    case MMC:
    {
      // translate the arguments here

      result = MMC_disk_read(buff, sector, count);

      // translate the reslut code here

      return result;
    }
  }
  return RES_PARERR;
}

/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if _USE_WRITE
DRESULT disk_write(BYTE pdrv, /* Physical drive nmuber (0..) */
    const BYTE *buff, /* Data to be written */
    DWORD sector, /* Sector address (LBA) */
    UINT count /* Number of sectors to write (1..128) */
)
{
  DRESULT result = RES_PARERR;

  switch (pdrv)
  {
    case SPI:
    {
      // translate the arguments here

//      result = SPI_disk_write(buff, sector, count);

      // translate the reslut code here
      break;
    }
    case MMC:
    {
      // translate the arguments here

      result = MMC_disk_write(buff, sector, count);

      // translate the reslut code here

      break;
    }
  }
  return result;
}
#endif

/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

#if _USE_IOCTL
DRESULT disk_ioctl(BYTE pdrv, /* Physical drive nmuber (0..) */
    BYTE cmd, /* Control code */
    void *buff /* Buffer to send/receive control data */
)
{
  DRESULT result = RES_PARERR;

  switch (pdrv)
  {
    case SPI:
    {
      // pre-process here

//      result = SPI_disk_ioctl(cmd, buff);

      // post-process here

      return result;
    }
    case MMC:
    {
      // pre-process here

      result = MMC_disk_ioctl(cmd, buff);

      // post-process here

      return result;
    }
  }
  return RES_PARERR;
}
#endif

DWORD get_fattime (void)
{
  time_t curtime;
  struct tm *loctime;

  /* Get the current time. */
  curtime = time (NULL);

  /* Convert it to local time representation. */
  loctime = localtime(&curtime);

  /* Returns current time packed into a DWORD variable */
  return
        ((DWORD)(loctime->tm_year + 1900 - 1980) << 25)   /* Year */
      | ((DWORD)(loctime->tm_mon + 1) << 21)              /* Month */
      | ((DWORD)(loctime->tm_mday) << 16)                 /* Mday  */
      | ((DWORD)(loctime->tm_hour) << 11)                 /* Hour */
      | ((DWORD)(loctime->tm_min) << 5)                   /* Min */
      | ((DWORD)(loctime->tm_sec / 2));                   /* Sec */
}

