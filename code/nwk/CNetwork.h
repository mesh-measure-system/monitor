/*
 * CNetwork.h
 *
 *  Created on: 1 paz 2014
 *      Author: Przemek
 */

#ifndef CNETWORK_H_
#define CNETWORK_H_

#include "CRun.h"
#include "CSettingsMgr.h"

#define CNET_DEFAULT_BEACON_ENDPOINT_ID   1
#define CNET_DEFAULT_BEACON_HANDLE        1

#define CNET_FH_CTRL_NO_COMMAND      0x00
#define CNET_FH_CTRL_BREAK_COMMAND   0x01
#define CNET_FH_CTRL_END_COMMAND     0x02
#define CNET_FH_CTRL_ASCII_COMMAND   0x04

typedef struct
{
  uint8_t				dstEndPoint;
  uint8_t				srcEndPoint;
  uint8_t       control;
}cnet_frame_header_t;



class CNetwork : public CRun
{
  private:
    CSetting<uint32_t, SETTING_UINT32> mac_hi;
    CSetting<uint32_t, SETTING_UINT32> mac_lo;

  public:

    void Init();
    virtual void run();
    virtual bool isIdle();


    virtual ~CNetwork() {};
};

extern CNetwork nwk;

#endif /* CNETWORK_H_ */
