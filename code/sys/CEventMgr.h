/*
 * CEventMgr.h
 *
 *  Created on: 14-11-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CEVENTMGR_H_
#define CEVENTMGR_H_

#include "CRun.h"
#include "stdint.h"

class CEvent
{
  private:
    CEvent* next_item_p;
  public:
    volatile uint8_t fired;

    virtual void callback_fire(uint8_t fired_cnt) = 0;
    virtual void fireEvent();

    friend class CEventMgr;

    // for eclipse happiness
    virtual ~CEvent();
};

class CEventMgr : public CRun
{
  private:
    CEvent* evt_list_p;
//    volatile bool main_fired;

  public:
    volatile bool main_fired;
    void Init();

    virtual bool isIdle();
    virtual void run();

    bool registerEvent(CEvent* evt_p);
    void unregisterEvent(CEvent* evt_p);
    void setFired();

    // for eclipse happiness
    virtual ~CEventMgr();
};

extern CEventMgr eventMgr;

#endif /* CEVENTMGR_H_ */
