/*
 * CPrint.h
 *
 *  Created on: 11-12-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CPRINT_H_
#define CPRINT_H_

#include "stdint.h"
#include "CFlashString.h"

class CPrint
{
  private:

  public:
    virtual char* getPrintfBuffer() = 0;
    virtual int   getPrintfBufferSize() = 0;
    virtual void write(char c) = 0;
    virtual void write(char* buff, int size);

    int printf(const char* format, ...);
    int printf(const __FlashStringHelper* format, ...);


    virtual ~CPrint() {};
};

#endif /* CPRINT_H_ */
