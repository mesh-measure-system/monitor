/*
 * halEeprom.h
 *
 *  Created on: 31-07-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef HALEEPROM_H_
#define HALEEPROM_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

#define   HAL_EEPROM_SIZE   8192


void      HAL_EepromInit(void);
uint16_t  HAL_EepromGetSize(void);
void      HAL_EepromWrite(uint16_t address, uint8_t data);
void      HAL_EepromWriteBytes(uint16_t address, uint8_t* data_p, uint16_t data_len);
uint8_t   HAL_EepromRead(uint16_t address);
void      HAL_EepromReadBytes(uint16_t address, uint8_t* data_p, uint16_t data_len);

#ifdef __cplusplus
}
#endif

#endif /* HALEEPROM_H_ */
