/*
 * CSettingsMgr.cpp
 *
 *  Created on: 20-11-2013
 *      Author: Przemek Kieszkowski
 */

#include <compiler.h>
#include "CSettingsMgr.h"
#include "sysTypes.h"
#include "halEeprom.h"
#include "crc7.h"
#include "string.h"

#if 1
#if defined(TRACE_LEVEL)
#undef  TRACE_LEVEL
#define TRACE_LEVEL TRACE_LEVEL_WARNING
#endif
#endif

#include "trace.h"

#define SYS_SETTINGS_VERSION        3
#define SYS_SETTINGS_VERSION_RESET  0

typedef struct PACK sys_settings_t
{
  uint8_t sett_version;
  uint8_t sett_size;
  uint8_t crc;
}sys_settings_t;



CSettingsMgr settingsMgr;

void CSettingsMgr::Init()
{
  sys_settings_t  settings;
  HAL_EepromReadBytes(0, (uint8_t*)&settings, sizeof(settings));
  TRACE_USER("settings.sett_size    = %d\n", (int)settings.sett_size);
  TRACE_USER("settings.sett_version = %d\n", (int)settings.sett_version);
  TRACE_USER("settings.crc          = 0x%x, calculated = 0x%x, size = %d\n", (int)settings.crc,
      (int)crc7(0, (uint8_t*)&settings, sizeof(settings) - sizeof(settings.crc)),
      (int)(sizeof(settings) - sizeof(settings.crc)));

  if ((settings.crc != crc7(0, (uint8_t*)&settings, sizeof(settings) - sizeof(settings.crc))) ||
      (settings.sett_version != SYS_SETTINGS_VERSION) ||
      (settings.sett_size != sizeof(sys_settings_t)))
  {
    uint16_t  address;

    TRACE_ERROR("Reinitialize all settings\n");

    // reinitialize all settings
    settings.sett_version = SYS_SETTINGS_VERSION;
    settings.sett_size = sizeof(sys_settings_t);
    settings.crc = crc7(0, (uint8_t*)&settings, sizeof(settings) - sizeof(settings.crc));
    HAL_EepromWriteBytes(0, (uint8_t*)&settings, sizeof(settings));


    for (address = sizeof(settings); address < HAL_EepromGetSize(); address++)
    {
      HAL_EepromWrite(address, SYS_SETTING_EMPTY);
    }
  }
}

void CSettingsMgr::FactorySettings(void)
{
  sys_settings_t  settings;
  TRACE_ERROR("Reinitialize all settings\n");

  // reinitialize all settings
  settings.sett_version = SYS_SETTINGS_VERSION_RESET;
  settings.sett_size = sizeof(sys_settings_t);
  settings.crc = 0;
  HAL_EepromWriteBytes(0, (uint8_t*)&settings, sizeof(settings));
}


bool CSettingBase::RegisterSetting(uint8_t maxLen, uint8_t* initialValue, uint8_t initialLen)
{
  sett_addr = SYS_SETTING_INVALID;

  if ((strlen(sett_name) > SYS_SETTING_MAX_NAME_LENGTH) ||
      (initialLen > SYS_SETTING_MAX_VALUE_LENGTH) ||
      (maxLen  > SYS_SETTING_MAX_VALUE_LENGTH) ||
      (initialLen > maxLen))
  {
    // setting name or value to long
    TRACE_ERROR("SYS_SettingRegister Error: %s, len = %d\n", sett_name, strlen(sett_name));
    return SYS_SETTING_INVALID;
  }

  if (FindSetting())
  {
    // setting found
    // return its idx
    TRACE_USER("Found setting type %d name %s, sett_addr = %d\n", (int)type, sett_name, sett_addr);
    return true;
  }
  else
  {
    // setting not found - create it
    // check if we have enough free space to create setting
    if ((sett_addr + sizeof(sys_setting_value_t) + strlen(sett_name) + maxLen) >= HAL_EepromGetSize())
    {
      // not enough free space in eeprom to create setting
      TRACE_ERROR("Not enough free space in eeprom to create setting %s\n", sett_name);
      return false;
    }

    sett_struct.setting_type = type;
    sett_struct.name_len = strlen(sett_name);
    sett_struct.value_len = maxLen;

    // write setting struct
    HAL_EepromWriteBytes(sett_addr, (uint8_t*)&sett_struct, sizeof(sett_struct));
    TRACE_DEBUG("Creating type %d, name_len %d, value_len %d\n", (int)sett_struct.setting_type , (int)sett_struct.name_len, (int)sett_struct.value_len);

    // write setting name
    HAL_EepromWriteBytes(sett_addr + sizeof(sett_struct), (uint8_t*)sett_name, sett_struct.name_len);
    TRACE_DEBUG("Setting name %s at address %d\n", sett_name, (int)(sett_addr + sizeof(sett_struct)));

    // write setting initial value
    HAL_EepromWriteBytes(sett_addr + sizeof(sett_struct) + sett_struct.name_len, (uint8_t*)initialValue, initialLen);
    TRACE_DEBUG("Created correctly setting %s, idx = %d, initialLen = %d\n", sett_name, sett_addr, (int)initialLen);
  }
  return true;
}



/*========================================================================================*/
// function returns setting idx (eeprom address)
// if setting found returns passed setting idx pointer true and setting idx (eeprom address)
// if setting not found returns NULL false and idx to the first free place in the eeprom (for store new setting)
bool CSettingBase::FindSetting()
{
  uint8_t name[SYS_SETTING_MAX_NAME_LENGTH + 1]; // +1 for ending zero

  TRACE_USER("FindSetting %s\n", sett_name);
  sett_addr = sizeof(sys_settings_t);
  do
  {
    TRACE_USER("FindSetting %s read from %d\n", sett_name, (int) sett_addr);
    HAL_EepromReadBytes(sett_addr, (uint8_t*)&sett_struct, sizeof(sett_struct));
    if ((sett_struct.setting_type == SYS_SETTING_EMPTY) ||
        (sett_struct.setting_type >= SYS_SETTING_LAST))
    {
      // end of stored setting list
      // in sett_idx_p->sett_addr is returned first free address to store new setting
      TRACE_ERROR("FindSetting %s not found\n", sett_name);
      return false;
    }

    // read setting name
    HAL_EepromReadBytes(sett_addr + sizeof(sett_struct), name, sett_struct.name_len);
    name[sett_struct.name_len] = 0;  // add ending zero

    TRACE_DEBUG("SYS_FindSetting %s, read name %s\n", sett_name, name);

    // compare setting name
    if (strncmp((char*)name, sett_name, SYS_SETTING_MAX_NAME_LENGTH) == 0)
    {
      // setting found
      TRACE_DEBUG("SYS_FindSetting found %s on idx = %d\n", sett_name, (int)sett_addr);
      return true;
    }

    sett_addr = sett_addr + getSettingStructSize();
  } while (sett_addr < HAL_EepromGetSize());

  sett_addr = SYS_SETTING_INVALID;
  return false; // zero means not found
}

void CSettingBase::readSetting(uint8_t* dst, uint8_t len)
{
  uint8_t max_read = 0;
  max_read = len < sett_struct.value_len ? len : sett_struct.value_len;
  HAL_EepromReadBytes(sett_addr + sizeof(sett_struct) + sett_struct.name_len, (uint8_t*)dst, max_read);
  TRACE_USER("SYS_SettingGetValue idx = %d\n", sett_addr);
}

void CSettingBase::writeSetting(uint8_t* src, uint8_t len)
{
  uint8_t buff_len = len < sett_struct.value_len ? len : sett_struct.value_len;
  HAL_EepromWriteBytes(sett_addr + sizeof(sett_struct) + sett_struct.name_len, src, buff_len);
  TRACE_USER("SYS_SettingSetValue idx = %d\n", sett_addr);

}

sett_addr_t CSettingBase::getSettingStructSize()
{
  return sizeof(sett_struct) + sett_struct.name_len + sett_struct.value_len;
}

void CSettingsMgr::print_trace(char* trace)
{
  UNUSED(trace);
  TRACE_USER("Called %s\n", trace);
}
