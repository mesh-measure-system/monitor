/*
 * CSys.cpp
 *
 *  Created on: 15-11-2013
 *      Author: Przemek Kieszkowski
 */

#include "CSys.h"
#include <compiler.h>
#include "trace.h"


// definition of the functions for happiness of the avr-g++
__extension__ typedef int __guard __attribute__((mode (__DI__)));

extern "C" int __cxa_guard_acquire(__guard *);
extern "C" void __cxa_guard_release (__guard *);
extern "C" void __cxa_guard_abort (__guard *);
extern "C" void __cxa_pure_virtual(void);

int __cxa_guard_acquire(__guard *g) {return !*(char *)(g);};
void __cxa_guard_release (__guard *g) {*(char *)g = 1;};
void __cxa_guard_abort (__guard *) {};
void __cxa_pure_virtual(void) {};


void operator delete( void* pf )
{
  UNUSED(pf);
  TRACE_FATAL("called delete operator !\n");
}

CSys::~CSys()
{
  // TODO Auto-generated destructor stub
}

