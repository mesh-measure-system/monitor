/*
 * CFatSDMain.h
 *
 *  Created on: 9 maj 2014
 *      Author: Przemek
 */

#ifndef CFATSDMAIN_H_
#define CFATSDMAIN_H_

#include "diskio.h"
#include "ff.h"

class CFatSDMain
{
  public:

    FATFS FatFs[_VOLUMES];    /* File system object for each logical drive */

    void Init();
    void InitCommands();

    virtual ~CFatSDMain() {};
};

extern CFatSDMain fatSDMgr;

extern FATFS MemoryFatFs;    /* FatFs work area needed for each volume */
extern FATFS uSDFatFs;       /* FatFs work area needed for each volume */


#endif /* CFATSDMAIN_H_ */
