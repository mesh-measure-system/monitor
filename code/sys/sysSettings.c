/*
 * sysSettings.c
 *
 *  Created on: 30-07-2013
 *      Author: Przemek Kieszkowski
 */

#include <stdbool.h>
#include <string.h>

#include "sysSettings.h"
#include "sysTypes.h"
#include "halEeprom.h"
#include "crc7.h"

#if 1
#if defined(TRACE_LEVEL)
#undef 	TRACE_LEVEL
#define TRACE_LEVEL TRACE_LEVEL_WARNING
#endif
#endif

#include "trace.h"

#define SYS_SETTINGS_VERSION  3

typedef struct PACK sys_settings_t
{
  uint8_t sett_version;
  uint8_t sett_size;
  uint8_t crc;
}sys_settings_t;

/** ------------------------------------------------ LOCAL FUNCTIONS -----------------------------*/
setting_idx_t* SYS_SettingRegister(setting_idx_t* idx_p, const char* settingName, uint8_t setting_type, uint8_t maxLen, uint8_t* initialValue, uint8_t initialLen);

/*========================================================================================*/
/*
 Function returns size of the passed setting
 */
static uint16_t SYS_SettingSize(sys_setting_value_t* setting_p)
{
	if (setting_p != NULL)
  {
  	return sizeof(sys_setting_value_t) + setting_p->name_len + setting_p->value_len;
	}
	return SYS_SETTING_INVALID;
}

/*========================================================================================*/
void SYS_SettingsInit(void)
{
  sys_settings_t  settings;
  HAL_EepromReadBytes(0, (uint8_t*)&settings, sizeof(settings));
  TRACE_USER("settings.sett_size    = %d\n", (int)settings.sett_size);
  TRACE_USER("settings.sett_version = %d\n", (int)settings.sett_version);
  TRACE_USER("settings.crc          = 0x%x, calculated = 0x%x, size = %d\n", (int)settings.crc, 
  		(int)crc7(0, (uint8_t*)&settings, sizeof(settings) - sizeof(settings.crc)),
  		(int)(sizeof(settings) - sizeof(settings.crc)));

  if ((settings.crc != crc7(0, (uint8_t*)&settings, sizeof(settings) - sizeof(settings.crc))) ||
      (settings.sett_version != SYS_SETTINGS_VERSION) ||
      (settings.sett_size != sizeof(sys_settings_t)))
  {
    uint16_t  address;

    TRACE_ERROR("Reinitialize all settings\n");

    // reinitialize all settings
    settings.sett_version = SYS_SETTINGS_VERSION;
    settings.sett_size = sizeof(sys_settings_t);
    settings.crc = crc7(0, (uint8_t*)&settings, sizeof(settings) - sizeof(settings.crc));
    HAL_EepromWriteBytes(0, (uint8_t*)&settings, sizeof(settings));


    for (address = sizeof(settings); address < HAL_EepromGetSize(); address++)
    {
      HAL_EepromWrite(address, SYS_SETTING_EMPTY);
    }
  }
}


/*========================================================================================*/
// function returns setting idx (eeprom address)
// if setting found returns passed setting idx pointer true and setting idx (eeprom address)
// if setting not found returns NULL false and idx to the first free place in the eeprom (for store new setting)
setting_idx_t* SYS_FindSetting(const char* settingName, setting_idx_t* sett_idx_p)
{
  uint8_t name[SYS_SETTING_MAX_NAME_LENGTH + 1]; // +1 for ending zero
  
  TRACE_USER("SYS_FindSetting %s\n", settingName);
  sett_idx_p->sett_addr = sizeof(sys_settings_t);
  do
  {
  	TRACE_USER("SYS_FindSetting %s read from %d\n", settingName, (int) sett_idx_p->sett_addr);
    HAL_EepromReadBytes(sett_idx_p->sett_addr, (uint8_t*)&(sett_idx_p->sett_struct), sizeof(sett_idx_p->sett_struct));
    if ((sett_idx_p->sett_struct.setting_type == SYS_SETTING_EMPTY) ||
        (sett_idx_p->sett_struct.setting_type >= SYS_SETTING_LAST))
    {
      // end of stored setting list
			// in sett_idx_p->sett_addr is returned first free address to store new settig
		  TRACE_ERROR("SYS_FindSetting %s not found\n", settingName);
      return SYS_SETTING_INVALID;
    }

    // read setting name
    HAL_EepromReadBytes(sett_idx_p->sett_addr + sizeof(sett_idx_p->sett_struct), name, sett_idx_p->sett_struct.name_len);
    name[sett_idx_p->sett_struct.name_len] = 0;  // add ending zero

		TRACE_USER("SYS_FindSetting %s, read name %s\n", settingName, name);
		
    // compare setting name
    if (strncmp((char*)name, settingName, SYS_SETTING_MAX_NAME_LENGTH) == 0)
    {
      // setting found
      TRACE_USER("SYS_FindSetting found %s on idx = %d\n", settingName, (int)sett_idx_p->sett_addr);
      return sett_idx_p;
    }

    sett_idx_p->sett_addr = sett_idx_p->sett_addr + SYS_SettingSize(&sett_idx_p->sett_struct);
  } while (sett_idx_p->sett_addr < HAL_EepromGetSize());

	sett_idx_p->sett_addr = SYS_SETTING_INVALID;
  return SYS_SETTING_INVALID; // zero means not found
}


/*========================================================================================*/
setting_idx_t* SYS_SettingRegister(setting_idx_t* idx_p, const char* settingName, uint8_t setting_type, uint8_t maxLen, uint8_t* initialValue, uint8_t initialLen)
{
	idx_p->sett_addr = SYS_SETTING_INVALID;
  
  if ((strlen(settingName) > SYS_SETTING_MAX_NAME_LENGTH) ||
  		(initialLen > SYS_SETTING_MAX_VALUE_LENGTH) ||
  		(maxLen  > SYS_SETTING_MAX_VALUE_LENGTH) ||
  		(initialLen > maxLen))
  {
    // setting name or value to long
    TRACE_ERROR("SYS_SettingRegister Error\n");
    return SYS_SETTING_INVALID;
  }

  if (SYS_FindSetting(settingName, idx_p))
  {
    // setting found
    // return its idx
    TRACE_USER("Found setting type %d name %s, sett_addr = %d\n", (int)setting_type, settingName, idx_p->sett_addr);
    return idx_p;
  }
  else
  {
    // setting not found - create it
    // check if we have enough free space to create setting
    if ((idx_p->sett_addr + sizeof(sys_setting_value_t) + strlen(settingName) + maxLen) >= HAL_EepromGetSize())
    {
      // not enought free space in eeprom to create setting
      TRACE_ERROR("Not enough free space in eeprom to create setting %s\n", settingName);
      return SYS_SETTING_INVALID;
    }

    idx_p->sett_struct.setting_type = setting_type;
    idx_p->sett_struct.name_len = strlen(settingName);
    idx_p->sett_struct.value_len = maxLen;

		// write setting struct
    HAL_EepromWriteBytes(idx_p->sett_addr, (uint8_t*)&idx_p->sett_struct, sizeof(idx_p->sett_struct));
    // write setting name
    HAL_EepromWriteBytes(idx_p->sett_addr + sizeof(idx_p->sett_struct), (uint8_t*)settingName, idx_p->sett_struct.name_len);
    // write setting initial value
    HAL_EepromWriteBytes(idx_p->sett_addr + sizeof(idx_p->sett_struct) + idx_p->sett_struct.name_len, (uint8_t*)initialValue, initialLen);
    TRACE_USER("Created correctly setting %s, idx = %d\n", settingName, idx_p->sett_addr);
  }
  return idx_p;
}

/*===================================================================================================*/
setting_idx_t* SYS_SettingNext(setting_idx_t* prev_idx_p)
{
	if (prev_idx_p->sett_addr == SYS_SETTING_INVALID)
	{
		// return first setting
	  prev_idx_p->sett_addr = sizeof(sys_settings_t);
	}
	else
	{
		// return next setting
	prev_idx_p->sett_addr = prev_idx_p->sett_addr + SYS_SettingSize(&(prev_idx_p->sett_struct));
	}

  HAL_EepromReadBytes(prev_idx_p->sett_addr, (uint8_t*)&(prev_idx_p->sett_struct), sizeof(sys_setting_value_t));
  if ((prev_idx_p->sett_struct.setting_type == SYS_SETTING_EMPTY) ||
      (prev_idx_p->sett_struct.setting_type >= SYS_SETTING_LAST))
  {
  	// no next settings found
  	prev_idx_p->sett_addr = SYS_SETTING_INVALID;
  	return SYS_SETTING_INVALID;
  }

	TRACE_USER("SYS_SettingNext returned %d\n", (int)prev_idx_p->sett_addr);
	return prev_idx_p;
}

/*========================================================================================*/
setting_idx_t*	SYS_SettingRegisterUInt8(setting_idx_t* idx_p, const char* settingName, uint8_t defaultValue)
{
	return SYS_SettingRegister(idx_p, settingName, SYS_SETTING_UINT8, sizeof(defaultValue), (uint8_t*)&defaultValue, sizeof(defaultValue));
}

/*========================================================================================*/
uint8_t SYS_SettingSetUInt8(setting_idx_t* idx_p, uint8_t value)
{
	return SYS_SettingSetValue(idx_p, (uint8_t*)&value, sizeof(value));
}

/*========================================================================================*/
uint8_t SYS_SettingGetUInt8(setting_idx_t* idx_p, uint8_t* value_p)
{
	return SYS_SettingGetValue(idx_p, (uint8_t*)value_p, sizeof(*value_p));
}

/*========================================================================================*/
setting_idx_t*	SYS_SettingRegisterUInt16(setting_idx_t* idx_p, const char* settingName, uint16_t defaultValue)
{
	return SYS_SettingRegister(idx_p, settingName, SYS_SETTING_UINT16, sizeof(defaultValue), (uint8_t*)&defaultValue, sizeof(defaultValue));
}

/*========================================================================================*/
uint8_t SYS_SettingSetUInt16(setting_idx_t* idx_p, uint16_t value)
{
	return SYS_SettingSetValue(idx_p, (uint8_t*)&value, sizeof(value));
}

/*========================================================================================*/
uint8_t SYS_SettingGetUInt16(setting_idx_t* idx_p, uint16_t* value_p)
{
	return SYS_SettingGetValue(idx_p, (uint8_t*)value_p, sizeof(*value_p));
}

/*========================================================================================*/
setting_idx_t*	SYS_SettingRegisterUInt32(setting_idx_t* idx_p, const char* settingName, uint32_t defaultValue)
{
	return SYS_SettingRegister(idx_p, settingName, SYS_SETTING_UINT32, sizeof(defaultValue), (uint8_t*)&defaultValue, sizeof(defaultValue));
}

/*========================================================================================*/
uint8_t SYS_SettingSetUInt32(setting_idx_t* idx_p, uint32_t value)
{
	return SYS_SettingSetValue(idx_p, (uint8_t*)&value, sizeof(value));
}

/*========================================================================================*/
uint8_t SYS_SettingGetUInt32(setting_idx_t* idx_p, uint32_t* value_p)
{
	return SYS_SettingGetValue(idx_p, (uint8_t*)value_p, sizeof(*value_p));
}

/*========================================================================================*/
setting_idx_t*	SYS_SettingRegisterString(setting_idx_t* idx_p, const char* settingName, uint8_t maxLen, char* defaultValue)
{
	return SYS_SettingRegister(idx_p, settingName, SYS_SETTING_STRING, maxLen, (uint8_t*)defaultValue, strlen(defaultValue) + 1);
}

/*========================================================================================*/
uint8_t SYS_SettingSetString(setting_idx_t* idx_p, char* value)
{
	uint16_t 	val_len = strlen(value) + 1;
  uint16_t 	buff_len = idx_p->sett_struct.value_len;
  uint8_t		ret;
  if (val_len <= buff_len)
  {
  	ret = SYS_SettingSetValue(idx_p, (uint8_t*)value, val_len);
  }
  else
  {
	  char			tmp = value[buff_len - 1];
  	value[buff_len - 1] = 0;
  	ret = SYS_SettingSetValue(idx_p, (uint8_t*)value, buff_len);	
  	value[buff_len - 1] = tmp;
	}
	
	return ret;
}

/*========================================================================================*/
uint8_t SYS_SettingGetString(setting_idx_t* idx_p, char* buff, uint8_t buff_size)
{
	return SYS_SettingGetValue(idx_p, (uint8_t*)buff, buff_size);
}

/*========================================================================================*/
/* function reads value of the given setting
   if the buffor size is smaller then setting value - there is read only a buff_size bytes.
   null character at the end is not added
   function returns number of the read bytes (0 if nothing was readed)
 */
uint8_t SYS_SettingGetValue(setting_idx_t* idx_p, uint8_t* buff, uint8_t buff_size)
{
  uint8_t max_read = 0;
	max_read = buff_size < idx_p->sett_struct.value_len ? buff_size : idx_p->sett_struct.value_len;
	HAL_EepromReadBytes(idx_p->sett_addr + sizeof(idx_p->sett_struct) + idx_p->sett_struct.name_len, (uint8_t*)buff, max_read);
	TRACE_USER("SYS_SettingGetValue idx = %d\n", idx_p->sett_addr);
  return max_read;
}

/*========================================================================================*/
/* function sets value of the given setting
   if the buffer size is smaller then setting value - there is read only a buff_size bytes.
   null character at the end is not added
   function returns number of the read bytes (0 if nothing was readed)
 */
uint8_t SYS_SettingSetValue(setting_idx_t* idx_p, uint8_t* buff, uint8_t buff_size)
{
  uint8_t buff_len = buff_size < idx_p->sett_struct.value_len ? buff_size : idx_p->sett_struct.value_len;
  HAL_EepromWriteBytes(idx_p->sett_addr + sizeof(idx_p->sett_struct) + idx_p->sett_struct.name_len, buff, buff_len);
  TRACE_USER("SYS_SettingSetValue idx = %d\n", idx_p->sett_addr);
  return buff_len;
}

/*========================================================================================*/
/* function reads name of the given setting
   if the buffor size is smaller then setting value - there is read only a buff_size bytes.
   null character at the end is not added
   function returns number of the read bytes (0 if nothing was readed)
 */
uint8_t SYS_SettingGetName(setting_idx_t* idx_p, char* buff, uint8_t buff_size)
{
  uint8_t max_read;

	if (buff_size > 2)
	{
		buff_size--;
		max_read = buff_size < idx_p->sett_struct.name_len ? buff_size : idx_p->sett_struct.name_len;
  	HAL_EepromReadBytes(idx_p->sett_addr + sizeof(idx_p->sett_struct), (uint8_t*)buff, max_read);
  	buff[max_read] = 0;
  	TRACE_USER("SYS_SettingGetName idx = %d name %s\n", idx_p->sett_addr, buff);
  }
  else
  {
  	TRACE_ERROR("SYS_SettingGetName buff_size to small (%d)\n", buff_size);
  	max_read = 0;
  }
  return max_read;
}
