/*
 * mmc_avr.h
 *
 *  Created on: 9 maj 2014
 *      Author: Przemek
 */

#ifndef MMC_AVR_H_
#define MMC_AVR_H_

/* MMC card type flags (MMC_GET_TYPE) */
#define CT_MMC    0x01    /* MMC ver 3 */
#define CT_SD1    0x02    /* SD ver 1 */
#define CT_SD2    0x04    /* SD ver 2 */
#define CT_SDC    (CT_SD1|CT_SD2) /* SD */
#define CT_BLOCK  0x08    /* Block addressing */

DSTATUS MMC_disk_initialize(void);

DSTATUS MMC_disk_status(void);

DRESULT MMC_disk_read(
    BYTE *buff, /* Pointer to the data buffer to store read data */
    DWORD sector, /* Start sector number (LBA) */
    UINT count /* Sector count (1..128) */
    );

DRESULT MMC_disk_write(
    const BYTE *buff, /* Pointer to the data to be written */
    DWORD sector, /* Start sector number (LBA) */
    UINT count /* Sector count (1..128) */
    );

DRESULT MMC_disk_ioctl(
    BYTE cmd, /* Control code */
    void *buff /* Buffer to send/receive control data */
    );

#endif /* MMC_AVR_H_ */
