/*
 * CApp.cpp
 *
 *  Created on: 07-11-2013
 *      Author: Przemek Kieszkowski
 */

#include "CApp.h"

#include <stdint.h>

#include "CCmdSetting.h"
#include "CCommandsMgr.h"
#include "CNetCmd.h"
#include "CUartMgr.h"
#include "halUart.h"
#include "CADC.h"
#include "CFlashString.h"
#include "CEventMgr.h"
#include "sys.h"
#include "CBattery.h"
#include "CLedMgr.h"
#include "CPowerMgr.h"
#include "CStratificationMgr.h"
#include "CNetCmdMgr.h"
#include "CToolCommands.h"
#include "CFatSDMain.h"
#include "CNetMgr.h"
#include "CNetwork.h"
#include "CSysWDT.h"
#include "CBTMgr.h"

#include "trace.h"

// TODO remove it when SW is fully adjusted to new BT
#define BT_WORKAROUND

// class instance
CApp app;

#ifdef BT_WORKAROUND
CPin pinBtPower;
#endif

void CApp::addToRun(CRun* run)
{
	run_classes.add_item(run);
}

//#define APP_NAME() ##APPNAME
void CApp::Init()
{
	run_classes.init();

	appState = APP_STATE_INITIAL;

	SYS_Init();

	TRACE_USER("-- Simlify Monitor --\n");
	TRACE_USER("-- Compiled: %s %s -:)-\n", __DATE__, __TIME__);

	fatSDMgr.Init();

	settingsMgr.Init();

	wdt_reset();
	eventMgr.Init();
	uartMgr.Init();
	adc.Init();

	cmdMgr.Init();

	sysWDT.Init();

	ledMgr.Init();

	wdt_reset();
	nwk.Init();
	netMgr.Init();

#ifdef BT_WORKAROUND // Temporary work around that activates bluetooth power supply
	pinBtPower.Init(CPin::PIN_PORTD, 4, CPin::PIN_OUT);
	pinBtPower.Set(false); // Turns BT power on
#endif
	BTMgr.Init();

	wdt_reset();
	powerMgr.Init();
	battMgr.Init();
#ifndef BT_WORKAROUND // TODO Stratification manager shouldn't be used on monitor HW - remove the module in the future (it controls BT power line)
	stratificationMgr.Init();
#endif

	netCmdMgr.Init();


	// commands registration
	toolCmd.Init();
	cmdSettingList.init((char*)CMD_SETTING_LIST_NAME);
	cmdSettingSet.init((char*)CMD_SETTING_SET_NAME);
	netCmd.init((char*)"net");
}

void CApp::TaskHandler()
{
	switch (appState)
	{
		case APP_STATE_INITIAL:
		{
			appState = APP_STATE_STARTED;
		}
		break;

		case APP_STATE_STARTED:
		{
		}
		break;
	}
}

void CApp::Run()
{
	while (1)
	{
		SYS_TaskHandler();
		HAL_UartTaskHandler();
		TaskHandler();

		// run method from classes
		CListIterator<CRun> run_iter(&run_classes);
		while(run_iter.get())
		{
			run_iter.get()->run();
			run_iter.next();
		}
	}
}
