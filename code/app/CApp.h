/*
 * CApp.h
 *
 *  Created on: 07-11-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CAPP_H_
#define CAPP_H_

#include "CRun.h"
#include "CSettingsMgr.h"
#include "CList.h"

#define CAPP_RUN_CLASSES_LIST   10

typedef enum AppState_t
{
	APP_STATE_INITIAL,
	APP_STATE_STARTED,
} AppState_t;


class CApp
{
	private:
		AppState_t appState;

	public:
		CList<CRun> run_classes;

		//    CRun*      runClasses[CAPP_RUN_CLASSES_LIST];

		void Init();
		void Run();
		void TaskHandler();
		void addToRun(CRun* run);
};

extern CApp app;

#endif /* CAPP_H_ */
