/**************************************************************************//**
  \file  halAdc.c

  \brief Implementation of hardware depended ADC interface.

  \author
      Atmel Corporation: http://www.atmel.com \n
      Support email: avr@atmel.com

    Copyright (c) 2008-2012, Atmel Corporation. All rights reserved.
    Licensed under Atmel's Limited License Agreement (BitCloudTM).

  \internal
    History:
      5/12/07 A. Khromykh - Created
 ******************************************************************************/
/******************************************************************************
 *   WARNING: CHANGING THIS FILE MAY AFFECT CORE FUNCTIONALITY OF THE STACK.  *
 *   EXPERT USERS SHOULD PROCEED WITH CAUTION.                                *
 ******************************************************************************/
#if defined(HAL_USE_ADC)
/******************************************************************************
                   Includes section
******************************************************************************/
#include <avr/io.h>
#include "sysclk.h"

#include "conf_clock.h"
#include "delay.h"

#include "hal.h"
#include "halRTC.h"
#include "sysclk.h"

#include "adc.h"

#include "trace.h"

/******************************************************************************
                   Defines section
******************************************************************************/
#define ALL_CHANNEL_MASK_LOW      0x1F
#define ALL_CHANNEL_MASK_HIGH     0x08

#define CHANNEL_MASK_1            0x01
#define CHANNEL_MASK_2            0x03
#define CHANNEL_MASK_3            0x04
#define CHANNEL_MASK_4            0x0C
#define DELAY_FOR_STABILIZE       125

/******************************************************************************
                   Constants section
******************************************************************************/
const uint8_t halAdcDivider1M[5] = {2, 3, 4, 5, 6};
const uint8_t halAdcDivider4M[5] = {2, 3, 4, 5, 6};
const uint8_t halAdcDivider8M[5] = {3, 4, 5, 6, 7};
const uint8_t halAdcDivider16M[5] = {4, 5, 6, 7, 8};

//uint8_t halAdcDivider*

/******************************************************************************
                   External variables section
******************************************************************************/
extern HAL_AdcDescriptor_t *adcDesc;

/******************************************************************************
                   Implementations section
******************************************************************************/
/**************************************************************************//**
\brief Set up parametres to ADC.
******************************************************************************/
void  halOpenAdc(void)
{
  sysclk_enable_peripheral_clock(&ADC);

  /* sets voltage reference */
  ADMUX = adcDesc->voltageReference;
  /* Enable left adjust result */
  if (RESOLUTION_8_BIT == adcDesc->resolution)
  {
    ADMUX |= (1 << ADLAR);
  }

  uint8_t tmp = halAdcDivider1M[adcDesc->sampleRate];
  ADCSRA = tmp | (1 << ADEN);
}

/**************************************************************************//**
\brief starts convertion on the ADC channel.
\param[in]
  channel - channel number.
******************************************************************************/
void halStartAdc(HAL_AdcChannelNumber_t channel)
{
  adcDesc->service.halAdcCurCount = 0;
  /* disable digital buffers */
  if (HAL_ADC_CHANNEL3 >= channel)
  {
    DIDR0 = (1 << channel);
  }

  uint8_t tmp = ADMUX & ALL_CHANNEL_MASK_LOW;
  tmp += ADCSRB & ALL_CHANNEL_MASK_HIGH ? 0x20 : 0;

  /* clear previous channel number */
  ADMUX  &= ~ALL_CHANNEL_MASK_LOW;
  ADCSRB &= ~ALL_CHANNEL_MASK_HIGH;

  /* set current channel number */
  ADMUX  |= (channel & ALL_CHANNEL_MASK_LOW);
  ADCSRB |= (channel & 0x20) ? 0x08 : 0;

  /* if new differential channel is settled then must make 125 us delay for gain stabilize. */
  if (tmp != channel)
  {
    delay_us(DELAY_FOR_STABILIZE);
  }

  // wait while AVDDOK is ok
  while((ADCSRB & (1 << AVDDOK)) == 0);

  while((ADCSRB & (1 << REFOK)) == 0);

  if (adcDesc->selectionsAmount > 1)
  {
    ADCSRA |= ((1 << ADIE)  | (1 << ADATE) | (1 << ADSC));  // Starts running mode
  }
  else
  {
    ADCSRA |= ((1 << ADIE) | (1 << ADSC)); // Starts one conversion
  }
//  ADCSRC = 
//  TRACE_USER("halStartAdc ADMUX=0x%02x, ADCSRA, ADCSRB\n", adcDesc->selectionsAmount);
}

/**************************************************************************//**
\brief Closes the ADC.
******************************************************************************/
void halCloseAdc(void)
{
//  TRACE_USER("halCloseAdc\n");
  ADMUX  = 0;
  ADCSRA = 0;
  // Digital input enable
  DIDR0 = 0;
  sysclk_disable_peripheral_clock(&ADC);
}

/**************************************************************************//**
\brief ADC conversion complete interrupt handler.
******************************************************************************/
ISR(ADC_vect)
{
  // Read ADC conversion result
  if (RESOLUTION_8_BIT == adcDesc->resolution)
  {
    ((uint8_t *)adcDesc->bufferPointer)[adcDesc->service.halAdcCurCount++] = ADCH;
  }
  else
  {
    ((uint16_t *)adcDesc->bufferPointer)[adcDesc->service.halAdcCurCount++] = ADC;
  }


//  HAL_UartWriteByte('X');

  if (adcDesc->service.halAdcCurCount == adcDesc->selectionsAmount)
  {
  // Disable ADC Interrupt
    ADCSRA &= ~(1 << ADIE);
    if (adcDesc->callback)
    {
      adcDesc->callback();
    }
  }
}
#endif // defined(HAL_USE_ADC)

// eof halAdc.c
